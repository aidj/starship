#ifndef OBSERVER_H
#define OBSERVER_H

#include "memory"

template<typename Event>
class Observer
{
public:
    virtual ~Observer() {}
    virtual void handleEvent(const Event& event) = 0;
};

template<typename Event>
using ObserverSPtr = std::shared_ptr<Observer<Event>>;

#endif // OBSERVER_H
