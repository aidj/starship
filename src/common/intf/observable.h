#ifndef OBSERVABLE_H
#define OBSERVABLE_H

#include <memory>
#include <list>

#include "observer.h"

template<typename Event>
class Observable
{
public:
    virtual ~Observable() {}
    void add(ObserverSPtr<Event> observer);
    void remove(ObserverSPtr<Event> observer);
    void notify(const Event& event);

private:
    std::list<ObserverSPtr<Event>> m_observers;
};

template<typename Event>
using ObservableSPtr = std::shared_ptr<Observable<Event>>;

template<typename Event>
void Observable<Event>::add(ObserverSPtr<Event> observer)
{
    m_observers.push_back(observer);
}

template<typename Event>
void Observable<Event>::remove(ObserverSPtr<Event> observer)
{
    m_observers.remove(observer);
}

template<typename Event>
void Observable<Event>::notify(const Event &event)
{
    for(auto& observer : m_observers) {
        observer->handleEvent(event);
    }
}

#endif // OBSERVABLE_H
