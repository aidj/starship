#ifndef ICREATOR_H
#define ICREATOR_H

#include <memory>

template<typename T, typename... Args>
class ICreator
{
public:
    virtual ~ICreator() {}

    virtual T create(Args... args) = 0;
};

template<typename T, typename... Args>
using ICreatorUPtr = std::unique_ptr<ICreator<T, Args...>>;

#endif // ICREATOR_H
