#ifndef IENGINE_H
#define IENGINE_H

class IEngine
{
public:
    virtual ~IEngine()  {}

    virtual void step() = 0;
};

#endif // IENGINE_H
