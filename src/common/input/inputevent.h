#ifndef INPUTEVENT_H
#define INPUTEVENT_H

#include "../intf/observable.h"

struct KeyData
{
    int key;
    bool isPressed;
};

struct MouseData
{
    int button;
    bool isPressed;
    unsigned int posX;
    unsigned int posY;
};

struct WheelData
{
    int scrollNumber;
};

union IputEventData
{
    KeyData keyData;
    MouseData mouseData;
    WheelData wheelData;
};

struct InputEvent
{   
    enum Type {KEY, MOUSE_CLICK, MOUSE_MOVE, MOUSE_WHEEL, MOUSE_DOUBLE_CLICK};

    Type type;
    IputEventData data;
};

using InputProvider = Observable<InputEvent>;
using InputProviderSPtr = ObservableSPtr<InputEvent>;
using IInputObserverSPtr = ObserverSPtr<InputEvent>;
using IInputObserver = Observer<InputEvent>;

#endif // INPUTEVENT_H
