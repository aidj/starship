#ifndef VEC2_H
#define VEC2_H

#include <CGAL/Cartesian.h>
using K = CGAL::Cartesian<float>;
using Point = K::Point_2;
using Direction2 = K::Direction_2;
using Vec2 = K::Vector_2;
using Transformation2 = CGAL::Aff_transformation_2<K>;

#endif // VEC2_H
