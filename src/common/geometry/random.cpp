#include "random.h"

#include <cstdlib>

float Random::random(float min, float max)
{
    return min + static_cast <float> (rand())
            /( static_cast <float> (RAND_MAX/(max - min)));
}

float Random::randomProbability()
{
    return static_cast <float> (rand())
            /( static_cast <float> (RAND_MAX));
}
