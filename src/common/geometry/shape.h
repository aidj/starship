#ifndef SHAPE_H
#define SHAPE_H

#include <vector>

#include "vec2.h"

struct Shape
{
    Shape() : points() {}
    Shape(const Shape& shape, float scaleFactor) : points() {
        Transformation2 scale(CGAL::SCALING, scaleFactor);
        for(const auto& point : shape.points) {
            points.push_back(scale(point));
        }
    }
    Shape(const Shape& shape) = default;
    Shape(std::vector<Point>&& _points) : points(_points) {}
    Shape(const std::vector<Point>& _points) : points(_points) {}

    void add(Point point)
    {
        points.push_back(point);
    }
    void scale(float scaleFactor) {
        Transformation2 scale(CGAL::SCALING, scaleFactor);
        for(auto& point : points) {
            point = scale(point);
        }
    }

    // Where (0,0) is a center of a shape mass
    std::vector<Point> points;
};

#endif // SHAPE_H
