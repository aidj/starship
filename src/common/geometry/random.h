#ifndef RANDOM_H
#define RANDOM_H

class Random
{
public:
    static float random(float min, float max);
    static float randomProbability();
};

#endif // RANDOM_H
