#ifndef VEC3_H
#define VEC3_H

#include <CGAL/Cartesian.h>

#include "vec2.h"

using K = CGAL::Cartesian<float>;
using Point3 = K::Point_3;
using Vec3 = K::Vector_3;
using Transformation3 = CGAL::Aff_transformation_3<K>;

#endif // VEC3_H
