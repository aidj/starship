#ifndef STARSHIPEXCEPTION_H
#define STARSHIPEXCEPTION_H

#include <exception>

#include <QDebug>

class StarshipException : public std::exception
{
public:
    StarshipException(std::string message)
        : std::exception(), m_message(std::move(message))
    {
        qDebug() << m_message.c_str();
    }

    const std::string& message() const
    {
        return m_message;
    }

private:
    std::string m_message;
};

#endif // STARSHIPEXCEPTION_H
