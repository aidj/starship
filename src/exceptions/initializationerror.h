#ifndef INITIALIZATIONERROR_H
#define INITIALIZATIONERROR_H

#include "starshipexception.h"

class InitializationError : public StarshipException
{
public:
    InitializationError(std::string message)
        : StarshipException(std::move(message)) {}
};

#endif // INITIALIZATIONERROR_H
