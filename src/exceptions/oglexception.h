#ifndef OGLEXCEPTION_H
#define OGLEXCEPTION_H

#include "starshipexception.h"

class OGLException : public StarshipException
{
public:
    OGLException(std::string message)
        : StarshipException(std::move(message)) {}
};

#endif // OGLEXCEPTION_H
