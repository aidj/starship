#ifndef INVALIDARGUMENT_H
#define INVALIDARGUMENT_H

#include "starshipexception.h"

class InvalidArgument : public StarshipException
{
public:
    InvalidArgument(std::string message)
        : StarshipException(std::move(message)) {}
};

#endif // INVALIDARGUMENT_H
