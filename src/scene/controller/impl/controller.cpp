#include "controller.h"

#include <QtCore>

#include "../../../common/geometry/random.h"

Controller::Controller(IModelSPtr model)
    : m_model(model)
{
}

void Controller::handleEvent(const InputEvent &event)
{
    switch (event.type) {
    case InputEvent::MOUSE_DOUBLE_CLICK:
        createAsteroids(Point(-5.0f, 2.9f), 100);
        break;
    case InputEvent::KEY:
        handleKeyEvent(event.data.keyData);
        break;
    default:
        break;
    }
}

void Controller::handleKeyEvent(const KeyData &keyData)
{
    // Here we don't check for nullptr,
    // because we suppose that starship always must be
    auto starship = m_model->getStarship();
    switch (keyData.key) {
    case Qt::Key_W:
        starship->turnThruster("bottom1", keyData.isPressed);
        starship->turnThruster("bottom2", keyData.isPressed);
        break;
    case Qt::Key_S:
        starship->turnThruster("top", keyData.isPressed);
        break;
    case Qt::Key_D:
        starship->turnThruster("left", keyData.isPressed);
        break;
    case Qt::Key_A:
        starship->turnThruster("right", keyData.isPressed);
        break;
    default:
        break;
    }
}

void Controller::createAsteroids(const Point& createPoint, unsigned int count)
{
    Shape shape1;
    shape1.add(Point(1.0f, 1.0f));
    shape1.add(Point(-1.0f, 1.0f));
    shape1.add(Point(-1.0f, -1.0f));
    shape1.add(Point(1.0f, -1.0f));

    AsteroidDef asteroidDef(AsteroidType::METAL, 0.5);
    RigidBodyDef rigidBodyDef(createPoint, 0.0f, Vec2(), 0.0f, std::move(shape1), 1.0f, 0.1f);
    for(auto i = 0u; i < count; ++i) {
        rigidBodyDef.linearVelocity =
                Vec2(Random::random(-2000.0f, 2000.0f),
                       Random::random(-2000.0f, 2000.0f));
        rigidBodyDef.angularVelocity =
                Random::random(-500.0f, 500.0f);
        m_model->addAsteroid(asteroidDef, rigidBodyDef);
    }
}
