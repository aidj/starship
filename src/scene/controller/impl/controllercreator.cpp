#include "controllercreator.h"

#include "controller.h"

IControllerSPtr ControllerCreator::create(IModelSPtr model)
{
    return std::make_shared<Controller>(model);
}
