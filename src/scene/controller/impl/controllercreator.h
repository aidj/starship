#ifndef CONTROLLERCREATOR_H
#define CONTROLLERCREATOR_H

#include "../../../common/intf/icreator.h"
#include "../intf/icontroller.h"
#include "../../model/intf/imodel.h"

class ControllerCreator : public ICreator<IControllerSPtr, IModelSPtr>
{
public:
    ControllerCreator() {}
    ~ControllerCreator() override {}

    // ICreator interface
    IControllerSPtr create(IModelSPtr model) override;
};

#endif // CONTROLLERCREATOR_H
