#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "../intf/icontroller.h"
#include "../../model/intf/imodel.h"

class Controller : public IController
{
public:
    Controller(IModelSPtr model);
    ~Controller() override {}

    // Observer interface
    void handleEvent(const InputEvent &event) override;

private:
    void handleKeyEvent(const KeyData& keyData);
    void createAsteroids(const Point& createPoint, unsigned int count);

    IModelSPtr m_model;
};

#endif // CONTROLLER_H
