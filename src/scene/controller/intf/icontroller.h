#ifndef ICONTROLLER_H
#define ICONTROLLER_H

#include <memory>

#include "../../../common/input/inputevent.h"

class IController : public IInputObserver
{
public:
    virtual ~IController() {}
};

using IControllerUPtr = std::unique_ptr<IController>;
using IControllerSPtr = std::shared_ptr<IController>;

#endif // ICONTROLLER_H
