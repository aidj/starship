#include "scene.h"

Scene::Scene(
        IModelSPtr model,
        IViewUPtr view,
        IControllerSPtr controller)
    : m_model(model),
      m_view(std::move(view)),
      m_controller(controller),
      m_runner(std::make_unique<SceneRunner>(this))
{
    m_view->getInputProvider()->add(m_controller);
}

void Scene::run()
{
    m_runner->run();
}

void Scene::step()
{
    m_model->step();
    m_view->update();
}
