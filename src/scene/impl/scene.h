#ifndef SCENE_H
#define SCENE_H

#include "../intf/iscene.h"
#include "../model/intf/imodel.h"
#include "../view/intf/iview.h"
#include "../controller/intf/icontroller.h"
#include "scenerunner.h"

class SceneRunner;

class Scene : public IScene
{
public:
    Scene(IModelSPtr model,
          IViewUPtr view,
          IControllerSPtr controller);
    ~Scene() override {}

    // IScene interface
    void run() override;
    void step();

private:
    IModelSPtr m_model;
    IViewUPtr m_view;
    IControllerSPtr m_controller;

    std::unique_ptr<SceneRunner> m_runner;
};

#endif // SCENE_H
