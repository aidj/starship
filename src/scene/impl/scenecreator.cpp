#include "scenecreator.h"

#include <memory>

#include "scene.h"

SceneCreator::SceneCreator(ICreatorUPtr<IModelSPtr> modelCreator,
        ICreatorUPtr<IViewUPtr, IModelSPtr> viewCreator,
        ICreatorUPtr<IControllerSPtr, IModelSPtr> controllerCreator)
    : m_modelCreator(std::move(modelCreator)),
      m_viewCreator(std::move(viewCreator)),
      m_controllerCreator(std::move(controllerCreator))
{
}

ISceneUPtr SceneCreator::create()
{
    auto model = m_modelCreator->create();
    return std::make_unique<Scene>(
                model,
                m_viewCreator->create(model),
                m_controllerCreator->create(model));
}
