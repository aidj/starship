#ifndef SCENERUNNER_H
#define SCENERUNNER_H

#include <QObject>
#include <QTimer>

#include "scene.h"

class Scene;

class SceneRunner : public QObject {
    Q_OBJECT
public:
    SceneRunner(Scene* scene);
    ~SceneRunner() override {
        // TODO: stop timer grasefully
    }

    void step();
    void run();

private:
    QTimer m_timer;
    Scene* m_scene;
};

#endif // SCENERUNNER_H
