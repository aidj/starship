#ifndef SCENECREATOR_H
#define SCENECREATOR_H

#include "../../common/intf/icreator.h"
#include "../intf/iscene.h"
#include "../model/intf/imodel.h"
#include "../controller/intf/icontroller.h"
#include "../view/intf/iview.h"

class SceneCreator : public ICreator<ISceneUPtr>
{
public:
    SceneCreator(
            ICreatorUPtr<IModelSPtr> modelCreator,
            ICreatorUPtr<IViewUPtr, IModelSPtr> viewCreator,
            ICreatorUPtr<IControllerSPtr, IModelSPtr> controllerCreator);
    ~SceneCreator() override {}

    // ICreator interface
    ISceneUPtr create() override;

private:
    ICreatorUPtr<IModelSPtr> m_modelCreator;
    ICreatorUPtr<IViewUPtr, IModelSPtr> m_viewCreator;
    ICreatorUPtr<IControllerSPtr, IModelSPtr> m_controllerCreator;
};

#endif // SCENECREATOR_H
