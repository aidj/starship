#include "scenerunner.h"

SceneRunner::SceneRunner(Scene* scene)
    : QObject(nullptr), m_timer(), m_scene(scene)
{}

void SceneRunner::step() {
    m_scene->step();
}

void SceneRunner::run() {
    QObject::connect(&m_timer, &QTimer::timeout,
                     this, &SceneRunner::step);
    m_timer.start(1000/240);
}
