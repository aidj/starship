#ifndef IVIEW_H
#define IVIEW_H

#include <memory>

#include "../../../common/input/inputevent.h"

class IView
{
public:
    virtual ~IView() {}

    virtual void update() = 0;
    virtual InputProviderSPtr getInputProvider() = 0;
};

using IViewUPtr = std::unique_ptr<IView>;

#endif // IVIEW_H
