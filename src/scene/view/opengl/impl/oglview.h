#ifndef OGLVIEW_H
#define OGLVIEW_H

#include "../../intf/iview.h"
#include "../../../model/intf/imodel.h"
#include "../widget/impl/oglwidget.h"
#include "../renderer/intf/ioglrenderer.h"

class OGLView : public IView
{
public:
    OGLView(IModelSPtr model);
    ~OGLView() override {}

    // IView interface
    void update() override;
    InputProviderSPtr getInputProvider() override;

    IWidgetSPtr getWidget();    

private:
    IWidgetSPtr m_widget;
};

using OGLViewSPtr = std::shared_ptr<OGLView>;

#endif // OGLVIEW_H
