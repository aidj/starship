#ifndef OGLVIEWCREATOR_H
#define OGLVIEWCREATOR_H

#include "../../../../common/intf/icreator.h"
#include "../../intf/iview.h"
#include "../../../model/intf/imodel.h"

class OGLViewCreator : public ICreator<IViewUPtr, IModelSPtr>
{
public:
    OGLViewCreator() {}
    ~OGLViewCreator() override {}

    // ICreator interface
    IViewUPtr create(IModelSPtr model) override;
};

#endif // OGLVIEWCREATOR_H
