#include "oglviewcreator.h"

#include <memory>

#include "oglview.h"

IViewUPtr OGLViewCreator::create(IModelSPtr model)
{
    auto oglView = std::make_unique<OGLView>(model);
    auto widget = oglView->getWidget();
    widget->enableLogging(true);
    widget->show();
    return oglView;
}
