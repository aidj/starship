#include "oglview.h"

#include "../renderer/impl/sceneoglrenderer.h"

OGLView::OGLView(IModelSPtr model)
    :  m_widget()
{
    m_widget = std::make_shared<OGLWidget>(512, 512);
    auto renderer = std::make_unique<SceneOGLRenderer>(
                IOGLContextProviderWPtr(m_widget),
                model);

    auto inputProvider = m_widget->getInputProvider();
    inputProvider->add(renderer->getInputObserver());

    m_widget->addRenderer(std::move(renderer));
}

void OGLView::update()
{
    m_widget->render();
}

InputProviderSPtr OGLView::getInputProvider()
{
    return m_widget->getInputProvider();
}

IWidgetSPtr OGLView::getWidget()
{
    return m_widget;
}
