#ifndef IOGLRENDERER_H
#define IOGLRENDERER_H

#include <memory>

#include "../../intf/ioglcontextprovider.h"
#include "../../../../../common/input/inputevent.h"
#include "../../../../model/intf/modelchangeevent.h"

class IOGLRenderer
{
public:
    virtual ~IOGLRenderer() {}

    virtual void render() = 0;
    virtual void resize(int width, int height) = 0;

    virtual IModelObserverSPtr getModelObserver() = 0;
    virtual IInputObserverSPtr getInputObserver() = 0;
};

using IOGLRendererUPtr = std::unique_ptr<IOGLRenderer>;

#endif // IOGLRENDERER_H
