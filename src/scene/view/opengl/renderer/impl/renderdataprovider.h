#ifndef RENDERDATAPROVIDER_H
#define RENDERDATAPROVIDER_H

#include <memory>

#include "renderdata.h"
#include "../../../../model/intf/modelchangeevent.h"

class RenderDataProvider : public IModelObserver
{
public:
    RenderDataProvider(RenderData& renderData);
    ~RenderDataProvider() override {}

    // Observer interface
    void handleEvent(const ModelChangeEvent &event) override;

private:
    RenderData& m_renderData;
};

using RenderDataProviderSPtr = std::shared_ptr<RenderDataProvider>;

#endif // RENDERDATAPROVIDER_H
