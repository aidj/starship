#include "sceneoglrenderer.h"

#include <math.h>

#include <QMatrix4x4>

#include "../../../../../exceptions/oglexception.h"

SceneOGLRenderer::SceneOGLRenderer(
        IOGLContextProviderWPtr contextProvider,
        IModelSPtr scene)
    : BaseOGLRenderer(contextProvider, getShadersDef()),
      m_scene(scene),
      m_renderData(m_shaderProgram, m_contextProvider, m_scene),
      m_renderDataProvider(),
      m_inputObserver()
{
    auto funcs = getFuncs();
    funcs->glEnable(GL_DEPTH_TEST);
    funcs->glDepthFunc (GL_LESS);

    funcs->glClearColor(0.2f, 0.0f, 0.5f, 1.0f);
    //funcs->glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    //funcs->glCullFace (GL_BACK); // cull back face

    m_renderDataProvider = std::make_shared<RenderDataProvider>(m_renderData);
    m_inputObserver = std::make_shared<SceneOGLRendererInputObserver>(m_renderData);
    m_scene->add(m_renderDataProvider);
}

SceneOGLRenderer::~SceneOGLRenderer()
{
    m_scene->remove(m_renderDataProvider);
}

void SceneOGLRenderer::render()
{
    auto funcs = getFuncs();
    // Clear the buffer with the current clearing color
    funcs->glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    static const GLfloat red[] = { 0.1f, 0.1f, 0.1f, 1.0f };
    funcs->glClearBufferfv(GL_COLOR, 0, red);

    if(!m_shaderProgram->bind()) {
        throw OGLException("Could not bind shader program. Log: " + m_shaderProgram->log().toStdString());
    }

    m_renderData.getCamera().nextFrame(m_renderData.getAspectRatio());
    QMatrix4x4 view = std::move(m_renderData.getCamera().getLookAt());
    QMatrix4x4 perspective;
    perspective.perspective(90, m_renderData.getAspectRatio(), 0.1f, -100.0f);

    for (auto& graphicalObj : m_renderData.getGraphicalObjects()) {
        graphicalObj->vao.bind();

        auto position = graphicalObj->gameObj->getRigidBody()->getPosition();
        QMatrix4x4 modelMatrix;
        modelMatrix.setToIdentity();
        modelMatrix.optimize();
        modelMatrix.translate(position.x(), position.y());
        modelMatrix.rotate(
                    graphicalObj->gameObj->getRigidBody()->getAngle() * 180.0 / s_pi,
                    QVector3D(0.0, 0.0, 1.0));

        QMatrix4x4 mvp = perspective * view * modelMatrix ;

        funcs->glUniformMatrix4fv(
                    s_transformMatrixLocation, 1, GL_FALSE, mvp.data());

        funcs->glDrawArrays( GL_LINE_LOOP, 0, graphicalObj->pointsCount);

        graphicalObj->vao.release();
    }

    m_shaderProgram->release();
}

void SceneOGLRenderer::resize(int width, int height)
{
    getFuncs()->glViewport(0, 0, width, height);
    m_renderData.setAspectRatio(width / height);
}

IModelObserverSPtr SceneOGLRenderer::getModelObserver()
{
    return m_renderDataProvider;
}

IInputObserverSPtr SceneOGLRenderer::getInputObserver()
{
    return m_inputObserver;
}

std::vector<BaseOGLRenderer::ShaderDef> SceneOGLRenderer::getShadersDef()
{
    std::vector<ShaderDef> shadersDef;
    shadersDef.push_back(
                ShaderDef(QOpenGLShader::Vertex,
                          "../../resources/graphics/shaders/scene_renderer/contour.vert"));
    shadersDef.push_back(
                ShaderDef(QOpenGLShader::Fragment,
                         "../../resources/graphics/shaders/scene_renderer/contour.frag"));

    return shadersDef;
}
