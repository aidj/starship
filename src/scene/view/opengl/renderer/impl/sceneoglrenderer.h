#ifndef SCENEOGLRENDERER_H
#define SCENEOGLRENDERER_H

#include <list>

#include "baseoglrenderer.h"
#include "../../../../model/intf/imodel.h"
#include "../../../../model/logic/obj/intf/igameobj.h"
#include "renderdata.h"
#include "renderdataprovider.h"
#include "sceneoglrendererinputobserver.h"

class SceneOGLRenderer : public BaseOGLRenderer
{
public:
    SceneOGLRenderer(
            IOGLContextProviderWPtr contextProvider,
            IModelSPtr scene);

    ~SceneOGLRenderer() override;

    // IOpenGLRenderer interface
    void render() override;
    void resize(int width, int height) override;
    IModelObserverSPtr getModelObserver() override;
    IInputObserverSPtr getInputObserver() override;

private:
    static const GLint s_transformMatrixLocation = 0;
    static constexpr double  s_pi = 3.141592653589793;

    std::vector<ShaderDef> getShadersDef();

    IModelSPtr m_scene;
    RenderData m_renderData;
    RenderDataProviderSPtr m_renderDataProvider;
    IInputObserverSPtr m_inputObserver;
};

#endif // SCENEOGLRENDERER_H
