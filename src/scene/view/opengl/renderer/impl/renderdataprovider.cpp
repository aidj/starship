#include "renderdataprovider.h"

RenderDataProvider::RenderDataProvider(RenderData& renderData)
    : m_renderData(renderData)
{
}

void RenderDataProvider::handleEvent(const ModelChangeEvent &event)
{
    switch(event.m_operation) {
    case ModelChangeEvent::CREATE:
        m_renderData.addSingle(event.m_gameObj);
        break;
    }
}
