#ifndef GRAPHICALOBJ_H
#define GRAPHICALOBJ_H

#include <memory>

#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>

#include "../../../../model/logic/obj/intf/igameobj.h"

struct GraphicalObj {
    QOpenGLBuffer vbo;
    QOpenGLVertexArrayObject vao;

    IGameObjSPtr gameObj;
    size_t pointsCount;
};

using GraphicalObjUPtr = std::unique_ptr<GraphicalObj>;

#endif // GRAPHICALOBJ_H
