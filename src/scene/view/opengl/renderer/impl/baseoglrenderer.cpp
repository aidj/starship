#include "baseoglrenderer.h"

#include <sstream>

#include "../../../../../exceptions/oglexception.h"
#include "../../../../../exceptions/initializationerror.h"

BaseOGLRenderer::BaseOGLRenderer(
        IOGLContextProviderWPtr contextProvider,
        const std::vector<ShaderDef>& shadersDef)
    : IOGLRenderer(),
      m_contextProvider(contextProvider),
      m_shaderProgram()
{
    logOpenGLGeneralInfo();
    initShaderProgram(shadersDef);
}

OGLFunctionsSPtr BaseOGLRenderer::getFuncs() {
    IOGLContextProviderSPtr contextProvider(m_contextProvider);
    return contextProvider->getFunctions();
}

void BaseOGLRenderer::initShaderProgram(const std::vector<ShaderDef>& shadersDef)
{
    if(shadersDef.empty()) {
        throw InitializationError("Empty shader definition list.");
    }

    IOGLContextProviderSPtr contextProvider(m_contextProvider);
    contextProvider->makeCurrentContext();
    m_shaderProgram = std::make_shared<QOpenGLShaderProgram>(nullptr);

    for(const auto &sharedDef: shadersDef) {
        if(!m_shaderProgram->addShaderFromSourceFile(
                    sharedDef.first,
                    QString::fromStdString(sharedDef.second))) {
            std::stringstream message;
            message <<"Could not compile shader. type: "
                   << sharedDef.first << ", path: " << sharedDef.second
                   << ". Log: " << m_shaderProgram->log().toStdString();
            throw OGLException(message.str());
        }
    }

    if(!m_shaderProgram->link()) {
        throw OGLException("Could not link shader program. Log: " + m_shaderProgram->log().toStdString());
    }

    contextProvider->doneCurrentContext();
}

void BaseOGLRenderer::logOpenGLGeneralInfo()
{
    auto funcs = getFuncs();
    qDebug() << "OpenGL information: VENDOR:       " << (const char*)funcs->glGetString(GL_VENDOR);
    qDebug() << "                    RENDERDER:    " << (const char*)funcs->glGetString(GL_RENDERER);
    qDebug() << "                    VERSION:      " << (const char*)funcs->glGetString(GL_VERSION);
    qDebug() << "                    GLSL VERSION: " << (const char*)funcs->glGetString(GL_SHADING_LANGUAGE_VERSION);
}
