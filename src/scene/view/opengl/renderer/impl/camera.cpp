#include "camera.h"

Camera::Camera()
    : m_center(0.0f, 0.0f, -0.1f),
      m_eye(0.0f, 0.0f, 1.0f),
      m_up(0.0f, 0.5f, 0.0f)
{
    memset(m_movementDirection, false, 4);
}

void Camera::nextFrame(float aspectRatio)
{
    auto translate = getTranslation();
    m_eye = translate(m_eye);
    m_center = translate(m_center);
}

void Camera::move(Camera::Direction direction, bool enable)
{
    m_movementDirection[direction] = enable;
}

void Camera::moveUp(float multiplier)
{
    // TODO: set limits for scrolling
    Transformation3 translate(
                CGAL::TRANSLATION,
                Vec3(0.0f, 0.0f, m_eye.z() * (multiplier - 1.0f)));
    m_eye = translate(m_eye);
}

QMatrix4x4 Camera::getLookAt() const
{
   QMatrix4x4 lookAt;
   lookAt.lookAt(
               QVector3D(m_eye.x(), m_eye.y(), m_eye.z()),
               QVector3D(m_center.x(), m_center.y(), m_center.z()),
               QVector3D(m_up.x(), m_up.y(), m_up.z()));
   return lookAt;
}

void Camera::setAspectRatio(float aspectRatio)
{
    m_aspectRatio = aspectRatio;
}

Transformation3 Camera::getTranslation()
{
    float moveLength = m_eye.z() / 20.0f;
    float xShift = 0.0f;
    float yShift = 0.0f;

    if(m_movementDirection[Camera::UP]) {
        yShift = moveLength;
    }
    if(m_movementDirection[Camera::DOWN]) {
        yShift = -moveLength;
    }
    if(m_movementDirection[Camera::RIGHT]) {
        xShift = moveLength * m_aspectRatio;
    }
    if(m_movementDirection[Camera::LEFT]) {
        xShift = -moveLength * m_aspectRatio;
    }
    return Transformation3(CGAL::TRANSLATION, Vec3(xShift, yShift, 0.0f));
}
