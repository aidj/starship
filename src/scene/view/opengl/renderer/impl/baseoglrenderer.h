#ifndef BASEOGLRENDERER_H
#define BASEOGLRENDERER_H

#include <memory>

#include <QOpenGLContext>
#include <QOpenGLFunctions_4_5_Core>

#include "../intf/ioglrenderer.h"
#include "../../widget/impl/oglwidget.h"
#include "../../intf/commonogltypes.h"

class BaseOGLRenderer : public IOGLRenderer
{
public:
    using ShaderDef = std::pair<QOpenGLShader::ShaderTypeBit, std::string>;

    BaseOGLRenderer(
            IOGLContextProviderWPtr contextProvider,
            const std::vector<ShaderDef>& shadersDef);
    ~BaseOGLRenderer() override {}

protected:
    OGLFunctionsSPtr getFuncs();
    void initShaderProgram(const std::vector<ShaderDef>& shadersDef);

    IOGLContextProviderWPtr m_contextProvider;
    ShaderProgramSPtr m_shaderProgram;

private:    
    void logOpenGLGeneralInfo();
};

#endif // BASEOGLRENDERER_H
