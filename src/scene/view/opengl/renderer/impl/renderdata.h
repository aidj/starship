#ifndef RENDERDATA_H
#define RENDERDATA_H

#include <memory>

#include "graphicalobj.h"
#include "../../../../model/intf/imodel.h"
#include "../../intf/ioglcontextprovider.h"
#include "../../intf/commonogltypes.h"
#include "camera.h"

class RenderData
{
public:
    RenderData(ShaderProgramSPtr shaderProgram,
               IOGLContextProviderWPtr contextProvider,
               IModelSPtr scene);
    ~RenderData();

    void addSingle(IGameObjSPtr gameObj);
    const std::list<GraphicalObjUPtr>& getGraphicalObjects() const;

    Camera& getCamera();

    float getAspectRatio() const;
    void setAspectRatio(float aspectRatio);

private:
    void add(IGameObjSPtr gameObj);
    QOpenGLBuffer createVBO(const float * pointsData, size_t size);

    ShaderProgramSPtr m_shaderProgram;
    IOGLContextProviderWPtr m_contextProvider;

    std::list<GraphicalObjUPtr> m_graphicalObjects;
    Camera m_camera;
    float m_aspectRatio;
};

#endif // RENDERDATA_H
