#include "renderdata.h"

#include <vector>

#include "../../../../../exceptions/oglexception.h"
#include "../../../../../common/geometry/vec2.h"

RenderData::RenderData(
        ShaderProgramSPtr shaderProgram,
        IOGLContextProviderWPtr contextProvider,
        IModelSPtr scene)
    : m_shaderProgram(shaderProgram),
      m_contextProvider(contextProvider),
      m_graphicalObjects(),
      m_camera(),
      m_aspectRatio(1.0)
{
    IOGLContextProviderSPtr sharedContextProvider( m_contextProvider );
    sharedContextProvider->makeCurrentContext();
    for(auto gameObj : scene->getGameObjs()) {
        add(gameObj);
    }
    sharedContextProvider->doneCurrentContext();
}

RenderData::~RenderData()
{
    for(auto& graphicalObj : m_graphicalObjects) {
        graphicalObj->vbo.destroy();
        graphicalObj->vao.destroy();
        graphicalObj->gameObj = nullptr;
    }
}

void RenderData::addSingle(IGameObjSPtr gameObj)
{
    IOGLContextProviderSPtr sharedContextProvider( m_contextProvider );
    sharedContextProvider->makeCurrentContext();
    add(gameObj);
    sharedContextProvider->doneCurrentContext();
}

const std::list<GraphicalObjUPtr> &RenderData::getGraphicalObjects() const
{
    return m_graphicalObjects;
}

Camera &RenderData::getCamera()
{
    return m_camera;
}

void RenderData::add(IGameObjSPtr gameObj) {
    auto graphicalObj = std::make_unique<GraphicalObj>();

    if(!graphicalObj->vao.create()) {
        throw OGLException("Could not create VAO for graphicalObj instance.");
    }
    graphicalObj->vao.bind();

    const std::vector<Point>& shape = gameObj->getRigidBody()->getShape().points;
    std::vector<float> buf;
    buf.resize(shape.size() * 2);
    for(auto i = 0; i < shape.size(); ++i) {
        buf[i * 2] = shape.at(i).x();
        buf[i * 2 + 1] = shape.at(i).y();
    }
    graphicalObj->vbo = createVBO(buf.data(), shape.size() * sizeof(float) * 2);
    graphicalObj->pointsCount = shape.size();

    m_shaderProgram->setAttributeBuffer(0, GL_FLOAT, 0, 2);
    m_shaderProgram->enableAttributeArray(0);

    /*m_contextProvider->getFunctions()->
            glVertexAttribPointer(
                graphicalObj->vbo.bufferId(), 2, GL_FLOAT, GL_FALSE, 0, NULL);
    m_contextProvider->getFunctions()->glEnableVertexAttribArray(0);*/

    graphicalObj->vbo.release();
    graphicalObj->vao.release();
    graphicalObj->gameObj = gameObj;
    m_graphicalObjects.push_back(std::move(graphicalObj));
}

float RenderData::getAspectRatio() const
{
    return m_aspectRatio;
}

void RenderData::setAspectRatio(float aspectRatio)
{
    m_aspectRatio = aspectRatio;
    m_camera.setAspectRatio(aspectRatio);
}

QOpenGLBuffer RenderData::createVBO(const float * pointsData, size_t size)
{
    QOpenGLBuffer vbo;
    if(!vbo.create()) {
        throw OGLException("Could not create vertex VBO.");
    }
    vbo.setUsagePattern(QOpenGLBuffer::StaticDraw);
    if(!vbo.bind())
    {
        vbo.destroy();
        throw OGLException("Could not bind vertex buffer to the context");
    }
    vbo.allocate(pointsData, size);

    return vbo;
}
