#include "sceneoglrendererinputobserver.h"

#include <cmath>

SceneOGLRendererInputObserver::SceneOGLRendererInputObserver(
        RenderData& renderData)
    : m_renderData(renderData)
{
}

void SceneOGLRendererInputObserver::handleEvent(const InputEvent &event)
{
    switch(event.type) {
    case InputEvent::MOUSE_WHEEL:
        moveUpCameraPosition(event.data.wheelData.scrollNumber);
        break;
    case InputEvent::KEY:
        moveCameraPosition(event.data.keyData);
        break;
    }
}
void SceneOGLRendererInputObserver::moveUpCameraPosition(int scrollNumber)
{
    if(scrollNumber > 0) {
        m_renderData.getCamera().moveUp(pow(2, scrollNumber));
    } else if(scrollNumber < 0) {
        m_renderData.getCamera().moveUp(pow(0.5, abs(scrollNumber)));
    }
}

void SceneOGLRendererInputObserver::moveCameraPosition(const KeyData &keyData)
{
    switch(keyData.key) {
    case Qt::Key_Left:
        m_renderData.getCamera().move(Camera::LEFT, keyData.isPressed);
        break;
    case Qt::Key_Right:
        m_renderData.getCamera().move(Camera::RIGHT, keyData.isPressed);
        break;
    case Qt::Key_Up:
        m_renderData.getCamera().move(Camera::UP, keyData.isPressed);
        break;
    case Qt::Key_Down:
        m_renderData.getCamera().move(Camera::DOWN, keyData.isPressed);
        break;
    default:
        break;
    }
}


