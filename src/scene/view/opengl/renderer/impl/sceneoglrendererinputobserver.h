#ifndef SCENEOGLRENDERERINPUTOBSERVER_H
#define SCENEOGLRENDERERINPUTOBSERVER_H

#include "../../../../../common/input/inputevent.h"
#include "renderdata.h"

class SceneOGLRendererInputObserver : public IInputObserver
{
public:
    SceneOGLRendererInputObserver(RenderData& renderData);
    ~SceneOGLRendererInputObserver() override {}

    // Observer interface
public:
    void handleEvent(const InputEvent &event) override;

private:
    void moveUpCameraPosition(int scrollNumber);
    void moveCameraPosition(const KeyData& keyData);

    RenderData& m_renderData;
};

using SceneRendererDataObserverSPtr = std::shared_ptr<SceneOGLRendererInputObserver>;

#endif // SCENEOGLRENDERERINPUTOBSERVER_H
