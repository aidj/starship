#ifndef CAMERA_H
#define CAMERA_H

#include <QMatrix4x4>

#include "../../../../../common/geometry/vec3.h"
#include "../../../../../common/geometry/vec2.h"

class Camera
{
public:
    enum Direction {UP, RIGHT, DOWN, LEFT};

    Camera();

    void nextFrame(float aspectRatio);
    void move(Direction direction, bool enable);
    void moveUp(float multiplier);
    QMatrix4x4 getLookAt() const;
    void setAspectRatio(float aspectRatio);

private:
    Transformation3 getTranslation();

    Point3 m_center;
    Point3 m_eye;
    Vec3 m_up;
    bool m_movementDirection[4];
    float m_aspectRatio;
};

#endif // CAMERA_H
