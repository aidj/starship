#ifndef COMMONOGLTYPES_H
#define COMMONOGLTYPES_H

#include <memory>

#include <QOpenGLShaderProgram>

using ShaderProgramSPtr = std::shared_ptr<QOpenGLShaderProgram>;

#endif // COMMONOGLTYPES_H
