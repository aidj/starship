#ifndef IOGLCONTEXTPROVIDER_H
#define IOGLCONTEXTPROVIDER_H

#include <memory>

#include <QOpenGLContext>
#include <QOpenGLFunctions_4_5_Core>

using OGLContextSPtr = std::shared_ptr<QOpenGLContext>;
using OGLFunctionsSPtr = std::shared_ptr<QOpenGLFunctions_4_5_Core>;

class IOGLContextProvider
{
public:
    virtual void makeCurrentContext() = 0;
    virtual void doneCurrentContext() = 0;
    virtual OGLFunctionsSPtr& getFunctions() = 0;
    virtual OGLContextSPtr& getContext() = 0;
};

using IOGLContextProviderSPtr = std::shared_ptr<IOGLContextProvider>;
using IOGLContextProviderWPtr = std::weak_ptr<IOGLContextProvider>;

#endif // IOGLCONTEXTPROVIDER_H
