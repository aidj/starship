#ifndef IWIDGET_H
#define IWIDGET_H

#include <memory>

#include "../../renderer/intf/ioglrenderer.h"
#include "../../intf/ioglcontextprovider.h"
#include "../../../../../exceptions/oglexception.h"
#include "../../intf/ioglcontextprovider.h"
#include "../../../../../common/input/inputevent.h"

class IWidget : public IOGLContextProvider
{
public:
    virtual void show() = 0;
    virtual void render() = 0;
    virtual void resize(unsigned int width, unsigned int height) = 0;
    virtual void enableLogging(bool enable) = 0;
    virtual void addRenderer(IOGLRendererUPtr renderer) = 0;
    virtual void removeRenderer() = 0;
    virtual InputProviderSPtr getInputProvider() = 0;
};

using IWidgetSPtr = std::shared_ptr<IWidget>;

#endif // IWIDGET_H
