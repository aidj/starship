#include "oglwidget.h"

#include <math.h>

#include <QSurfaceFormat>
#include <qlogging.h>
#include <QDateTime>

#include "../../../../../exceptions/invalidargument.h"

OGLWidget::OGLWidget(unsigned int width, unsigned int height)
    : InputProviderWidget(),
      m_context(),
      m_funcs(),      
      m_renderer(),
      m_size(),
      m_needToResize(false),
      m_logger()
{
    init();
    resize(width, height);
}

void OGLWidget::makeCurrentContext()
{
    if(!m_context->makeCurrent(this)) {
        throw OGLException("Could not make current OpenGL context.");
    }
}

void OGLWidget::doneCurrentContext()
{
    m_context->doneCurrent();
}

OGLFunctionsSPtr& OGLWidget::getFunctions()
{
    return m_funcs;
}

OGLContextSPtr &OGLWidget::getContext()
{
    return m_context;
}

void OGLWidget::show()
{
    QWindow::show();
}

void OGLWidget::render()
{
    if(wasExposed()) {
        makeCurrentContext();
        m_renderer->render();
        m_context->swapBuffers(this);
        doneCurrentContext();
    }
}

void OGLWidget::resizeEvent(QResizeEvent *ev)
{
    resize(ev->size().width(), ev->size().height());
}

void OGLWidget::resize(unsigned int width, unsigned int height)
{
    if(width < 1 || height < 1) {
        throw InvalidArgument("OpenGLWidget should have positive width and height.");
    }

    if(width == m_size.first || height == m_size.second) {
        return;
    }

    m_size.first = width;
    m_size.second = height;

    resize();
}

void OGLWidget::resize()
{
    if(!wasExposed()) {
        m_needToResize = true;
        return;
    }

    QWindow::resize(m_size.first, m_size.second);
    makeCurrentContext();
    m_renderer->resize(m_size.first, m_size.second);
    doneCurrentContext();
}

void OGLWidget::enableLogging(bool enable)
{
    if(enable && !m_logger) {
        m_logger = std::make_unique<OGLLogger>(this);
    } else {
        m_logger = nullptr;
    }
}

void OGLWidget::addRenderer(IOGLRendererUPtr renderer)
{
    m_renderer = std::move(renderer);
    m_renderer->resize(m_size.first, m_size.second);
}

void OGLWidget::removeRenderer()
{
    m_renderer = nullptr;
}

void OGLWidget::exposeEvent(QExposeEvent *ev)
{
    if(isExposed()) {
        if(m_needToResize) {
            resize();
        }
        m_needToResize = false;
    }
}

bool OGLWidget::wasExposed()
{
    if(!isExposed()) {
        qWarning() << "Window isn't exposed.";
        return false;
    }
    return true;
}

void OGLWidget::initFuncs()
{
    auto funcs = m_context->versionFunctions<QOpenGLFunctions_4_5_Core>();
    if (funcs == nullptr) {
        throw OGLException("Could not get OpenGL function pointer");
    }
    m_funcs = std::shared_ptr<QOpenGLFunctions_4_5_Core>(funcs);
    if(!m_funcs->initializeOpenGLFunctions()) {
        throw OGLException("Could not initialize OpenGL function pointer");
    }
}

void OGLWidget::init()
{
    // Tell Qt we will use OpenGL for this window
    setSurfaceType( OpenGLSurface );

    // Specify the format and create platform-specific surface
    QSurfaceFormat format;
    format.setDepthBufferSize( 24 );
    format.setMajorVersion( 4 );
    format.setMinorVersion( 5 );
    format.setSamples( 4 );
    format.setProfile( QSurfaceFormat::CoreProfile );
    format.setOption(QSurfaceFormat::DebugContext);
    setFormat( format );
    create();

    m_context = std::make_shared<QOpenGLContext>(nullptr);
    m_context->setFormat( format );
    if(!m_context->create()) {
        throw OGLException("Could not create OpenGL context.");
    }
    makeCurrentContext();

    qDebug() << "Used OpenGL: " << m_context->format().majorVersion()
             << "." << m_context->format().minorVersion();

    initFuncs();
    doneCurrentContext();
}
