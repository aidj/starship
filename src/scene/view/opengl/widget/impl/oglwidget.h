#ifndef OGLWIDGET_H
#define OGLWIDGET_H

#include <memory>

#include <QWindow>
#include <QOpenGLContext>
#include <QOpenGLFunctions_4_5_Core>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLShaderProgram>
#include <QOpenGLDebugMessage>
#include <QString>
#include <QResizeEvent>

#include "../../renderer/intf/ioglrenderer.h"
#include "../../intf/ioglcontextprovider.h"
#include "../../../../../exceptions/oglexception.h"
#include "ogllogger.h"
#include "../intf/iwidget.h"
#include "inputproviderwidget.h"

class OGLWidget : public InputProviderWidget
{
    Q_OBJECT
public:
    OGLWidget(unsigned int width, unsigned int height);
    ~OGLWidget() override {}

    // IOpenGLRenderer interface
    void makeCurrentContext() override;
    void doneCurrentContext() override;
    OGLFunctionsSPtr& getFunctions() override;
    OGLContextSPtr& getContext() override;

    // IWidget interface
    void show() override;
    void render() override;
    void resize(unsigned int width, unsigned int height) override;
    void enableLogging(bool enable) override;
    void addRenderer(IOGLRendererUPtr renderer) override;
    void removeRenderer() override;

protected:
    void exposeEvent(QExposeEvent *ev) override;
    void resizeEvent(QResizeEvent *ev) override;

private:
    bool wasExposed();
    void resize();
    void init();
    void initFuncs();
    void deinit();

    OGLContextSPtr m_context;
    OGLFunctionsSPtr m_funcs;

    IOGLRendererUPtr m_renderer;
    std::pair<unsigned int, unsigned int> m_size;
    bool m_needToResize;

    OGLLoggerUPtr m_logger;
};

using OpenGLWidgetSPtr = std::shared_ptr<OGLWidget>;

#endif // OGLWIDGET_H
