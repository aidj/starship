#include "ogllogger.h"

const char* OGLLogger::s_extensionName = "GL_KHR_debug";

OGLLogger::OGLLogger(IOGLContextProvider* contextProvider)
    : QObject(nullptr)
{
    contextProvider->makeCurrentContext();
    if(!contextProvider->getContext()->hasExtension(QByteArrayLiteral("GL_KHR_debug"))) {
        throw OGLException(std::string("Extension ") + s_extensionName + " isn't supported.");
    }

    m_logger = std::make_unique<QOpenGLDebugLogger>(nullptr);
    m_logger->initialize();
    connect(m_logger.get(), &QOpenGLDebugLogger::messageLogged,
            this, &OGLLogger::log);
    m_logger->startLogging();
    contextProvider->doneCurrentContext();
}

OGLLogger::~OGLLogger()
{
    m_logger->stopLogging();
    disconnect(m_logger.get(), &QOpenGLDebugLogger::messageLogged, this,
               &OGLLogger::log);
    m_logger = nullptr;
}

void OGLLogger::log(const QOpenGLDebugMessage &debugMessage)
{
    qDebug() << debugMessage;
}
