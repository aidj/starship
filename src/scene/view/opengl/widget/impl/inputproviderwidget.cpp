#include "inputproviderwidget.h"

#include "QDebug"

InputProviderWidget::InputProviderWidget()
    : QWindow(), m_inputProvider(std::make_shared<InputProvider>())
{
}

InputProviderSPtr InputProviderWidget::getInputProvider()
{
    return m_inputProvider;
}

void InputProviderWidget::keyPressEvent(QKeyEvent *ev)
{
    if(!ev->isAutoRepeat()) {
        qDebug() << "press";
        InputEvent event;
        event.type = InputEvent::KEY;
        event.data.keyData.isPressed = true;
        event.data.keyData.key = ev->key();
        m_inputProvider->notify(event);
    }

    ev->accept();
}

void InputProviderWidget::keyReleaseEvent(QKeyEvent *ev)
{
    if(!ev->isAutoRepeat()) {
        qDebug() << "release";
        InputEvent event;
        event.type = InputEvent::KEY;
        event.data.keyData.isPressed = false;
        event.data.keyData.key = ev->key();
        m_inputProvider->notify(event);
    }

    ev->accept();
}

void InputProviderWidget::mouseDoubleClickEvent(QMouseEvent *ev)
{
    InputEvent event;
    event.type = InputEvent::MOUSE_DOUBLE_CLICK;
    event.data.mouseData.button = ev->button();
    event.data.mouseData.posX = ev->x();
    event.data.mouseData.posY = ev->y();
    m_inputProvider->notify(event);

    ev->accept();
}

void InputProviderWidget::mouseMoveEvent(QMouseEvent *ev)
{
    InputEvent event;
    event.type = InputEvent::MOUSE_MOVE;
    event.data.mouseData.posX = ev->x();
    event.data.mouseData.posY = ev->y();
    m_inputProvider->notify(event);

    ev->accept();
}

void InputProviderWidget::mousePressEvent(QMouseEvent *ev)
{
    InputEvent event;
    event.type = InputEvent::MOUSE_CLICK;
    event.data.mouseData.isPressed = true;
    event.data.mouseData.button = ev->button();
    event.data.mouseData.posX = ev->x();
    event.data.mouseData.posY = ev->y();
    m_inputProvider->notify(event);

    ev->accept();
}

void InputProviderWidget::mouseReleaseEvent(QMouseEvent *ev)
{
    InputEvent event;
    event.type = InputEvent::MOUSE_CLICK;
    event.data.mouseData.isPressed = false;
    event.data.mouseData.button = ev->button();
    event.data.mouseData.posX = ev->x();
    event.data.mouseData.posY = ev->y();
    m_inputProvider->notify(event);

    ev->accept();
}

void InputProviderWidget::wheelEvent(QWheelEvent *ev)
{
    InputEvent event;
    event.type = InputEvent::MOUSE_WHEEL;
    event.data.wheelData.scrollNumber = ev->angleDelta().y() / 8 / 15;
    m_inputProvider->notify(event);

    ev->accept();
}
