#ifndef INPUTPROVIDERWIDGET_H
#define INPUTPROVIDERWIDGET_H

#include <QWindow>
#include <QKeyEvent>

#include "../intf/iwidget.h"
#include "../../../../../common/input/inputevent.h"

class InputProviderWidget : public QWindow, public IWidget
{
    Q_OBJECT
public:
    InputProviderWidget();
    ~InputProviderWidget() override {}

    InputProviderSPtr getInputProvider() override;

protected:
    void keyPressEvent(QKeyEvent *ev) override;
    void keyReleaseEvent(QKeyEvent *ev) override;
    void mouseDoubleClickEvent(QMouseEvent *ev) override;
    void mouseMoveEvent(QMouseEvent *ev) override;
    void mousePressEvent(QMouseEvent *ev) override;
    void mouseReleaseEvent(QMouseEvent *ev) override;
    void wheelEvent(QWheelEvent *ev) override;

private:
    InputProviderSPtr m_inputProvider;
};

#endif // INPUTPROVIDERWIDGET_H
