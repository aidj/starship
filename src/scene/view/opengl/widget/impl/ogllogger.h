#ifndef OGLLOGGER_H
#define OGLLOGGER_H

#include <QObject>
#include <QOpenGLDebugLogger>

#include "../../intf/ioglcontextprovider.h"
#include "../../../../../exceptions/oglexception.h"

class OGLLogger : public QObject
{
    Q_OBJECT
public:
    OGLLogger(IOGLContextProvider* contextProvider);
    ~OGLLogger() override;

private slots:
    void log(const QOpenGLDebugMessage &debugMessage);

private:
    static const char* s_extensionName;

    std::unique_ptr<QOpenGLDebugLogger> m_logger;
};

using OGLLoggerUPtr = std::unique_ptr<OGLLogger>;

#endif // OGLLOGGER_H
