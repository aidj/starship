#ifndef MODELCHANGEEVENT_H
#define MODELCHANGEEVENT_H

#include "../logic/obj/intf/igameobj.h"
#include "../../../common/intf/observable.h"

struct ModelChangeEvent
{
public:
    enum Operation {CREATE, UPDATE, REMOVE};

    ModelChangeEvent(IGameObjSPtr gameObj, Operation operation)
        : m_gameObj(gameObj), m_operation(operation)
    {}

    IGameObjSPtr m_gameObj;
    Operation m_operation;
};

using ModelProviderSPtr = ObservableSPtr<ModelChangeEvent>;
using IModelObserverSPtr = ObserverSPtr<ModelChangeEvent>;
using IModelObserver = Observer<ModelChangeEvent>;

#endif // MODELCHANGEEVENT_H
