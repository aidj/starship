#ifndef IMODEL_H
#define IMODEL_H

#include <memory>
#include <deque>

#include "../logic/obj/intf/igameobj.h"
#include "../logic/obj/space/intf/asteroidtype.h"
#include "../../../common/intf/iengine.h"
#include "../physics/intf/rigidbodydef.h"
#include "../../../common/input/inputevent.h"
#include "modelchangeevent.h"
#include "../logic/obj/space/intf/asteroiddef.h"
#include "../logic/obj/startship/impl/starship.h"

class IModel : public IEngine, public Observable<ModelChangeEvent>
{
public:
    IModel() : IEngine() {}
    ~IModel() override {}

    virtual void addAsteroid(const AsteroidDef& asteroidDef, const RigidBodyDef& rigidBodyDef) = 0;
    virtual void addStarship(const RigidBodyDef&) = 0;
    virtual StarshipSPtr getStarship() = 0;

    virtual const std::deque<IGameObjSPtr>& getGameObjs() = 0;
};

using IModelSPtr = std::shared_ptr<IModel>;
using IModelUPtr = std::unique_ptr<IModel>;
using IModelWPtr = std::weak_ptr<IModel>;

#endif // IMODEL_H

