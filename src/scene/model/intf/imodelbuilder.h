#ifndef IMODELBUILDER_H
#define IMODELBUILDER_H

#include "imodel.h"

class IModelBuilder
{
public:
    virtual ~IModelBuilder() {}

    virtual void build(IModelSPtr model) = 0;
};

using IModelBuilderUPtr = std::unique_ptr<IModelBuilder>;

#endif // IMODELBUILDER_H
