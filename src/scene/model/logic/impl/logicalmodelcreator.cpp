#include "logicalmodelcreator.h"

#include "logicalmodel.h"

LogicalModelCreator::LogicalModelCreator()
{
}

ILogicalModelUPtr LogicalModelCreator::create()
{
    return std::make_unique<LogicalModel>();
}
