#include "logicalmodel.h"

#include <QDebug>

LogicalModel::LogicalModel()
    : ILogicalModel(),
      m_gameObjs(),
      m_startShip()
{
}

IGameObjSPtr LogicalModel::add(IGameObjUPtr gameObj)
{
    auto sharedGameObj = std::shared_ptr<IGameObj>(std::move(gameObj));
    m_gameObjs.push_back(sharedGameObj);
    return sharedGameObj;
}

const std::deque<IGameObjSPtr> &LogicalModel::getGameObjs()
{
    return m_gameObjs;
}

StarshipSPtr LogicalModel::setStarship(StarshipUPtr startShip)
{
    auto startShipSPtr = std::shared_ptr<Starship>(std::move(startShip));
    m_gameObjs.push_back(startShipSPtr);
    m_startShip = startShipSPtr;
    return m_startShip;
}

StarshipSPtr LogicalModel::getStarship()
{
    return m_startShip;
}

void LogicalModel::step()
{
    m_startShip->step();
}
