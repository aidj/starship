#ifndef LOGICALMODELCREATOR_H
#define LOGICALMODELCREATOR_H

#include "../intf/ilogicalmodel.h"
#include "../../../../common/intf/icreator.h"

class LogicalModelCreator : public ICreator<ILogicalModelUPtr>
{
public:
    LogicalModelCreator();
    ~LogicalModelCreator() override {}

    // ICreator interface
    ILogicalModelUPtr create() override;
};

#endif // LOGICALMODELCREATOR_H
