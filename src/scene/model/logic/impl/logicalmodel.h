#ifndef LOGICALMODEL_H
#define LOGICALMODEL_H

#include <deque>

#include "../intf/ilogicalmodel.h"
#include "../obj/intf/igameobj.h"

class LogicalModel : public ILogicalModel
{
public:
    LogicalModel();
    ~LogicalModel() override {}

    // ILogicScene interface
    IGameObjSPtr add(IGameObjUPtr gameObj) override;
    const std::deque<IGameObjSPtr>& getGameObjs() override;
    StarshipSPtr setStarship(StarshipUPtr startShip) override;
    StarshipSPtr getStarship() override;

    // IEngine interface
    void step() override;

private:
    std::deque<IGameObjSPtr> m_gameObjs;
    StarshipSPtr m_startShip;
};

#endif // LOGICALMODEL_H
