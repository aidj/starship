#ifndef ILOGICALSCENE_H
#define ILOGICALSCENE_H

#include <deque>

#include "../obj/intf/igameobj.h"
#include "../../../../common/intf/iengine.h"
#include "../obj/startship/impl/starship.h"

class ILogicalModel : public IEngine
{
public:
    ILogicalModel() : IEngine() {}
    ~ILogicalModel() override {}

    virtual IGameObjSPtr add(IGameObjUPtr gameObj) = 0;
    virtual const std::deque<IGameObjSPtr>& getGameObjs() = 0;
    virtual StarshipSPtr setStarship(StarshipUPtr gameObj) = 0;
    virtual StarshipSPtr getStarship() = 0;
};

using ILogicalModelUPtr = std::unique_ptr<ILogicalModel>;

#endif // ILOGICALSCENE_H
