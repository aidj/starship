#ifndef IGAMEOBJ_H
#define IGAMEOBJ_H

#include <memory>

#include "../../../physics/intf/irigidbody.h"

class IGameObj
{
public:
    virtual ~IGameObj() {}

    virtual bool hasRigidBody() const = 0;
    virtual IRigidBodySPtr getRigidBody() = 0;
};

using IGameObjSPtr = std::shared_ptr<IGameObj>;
using IGameObjUPtr = std::unique_ptr<IGameObj>;

#endif // IGAMEOBJ_H
