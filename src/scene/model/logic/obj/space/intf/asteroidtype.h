#ifndef ASTEROIDTYPE_H
#define ASTEROIDTYPE_H

enum AsteroidType {
    CARBON, SILICATE, METAL
};

#endif // ASTEROIDTYPE_H
