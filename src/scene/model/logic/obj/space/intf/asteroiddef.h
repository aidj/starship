#ifndef ASTEROIDDEF_H
#define ASTEROIDDEF_H

#include "asteroidtype.h"

struct AsteroidDef
{
    AsteroidDef() = default;
    AsteroidDef(AsteroidType _type, float _albedo)
        : type(_type), albedo(_albedo) {}

    AsteroidType type;
    float albedo;    
};

#endif // ASTEROIDDEF_H
