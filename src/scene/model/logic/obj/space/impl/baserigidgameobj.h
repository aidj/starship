#ifndef BASERIGIDGAMEOBJ_H
#define BASERIGIDGAMEOBJ_H

#include "../../intf/igameobj.h"

class BaseRigidGameObj : public IGameObj
{
public:
    BaseRigidGameObj(IRigidBodySPtr rigidBody);
    ~BaseRigidGameObj() override {}

    // IGameObj interface
public:
    bool hasRigidBody() const override;
    IRigidBodySPtr getRigidBody() override;

protected:
    IRigidBodySPtr m_rigidBody;
};

#endif // BASERIGIDGAMEOBJ_H
