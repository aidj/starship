#include "baserigidgameobj.h"

BaseRigidGameObj::BaseRigidGameObj(IRigidBodySPtr rigidBody)
    : m_rigidBody(std::move(rigidBody))
{
}

bool BaseRigidGameObj::hasRigidBody() const
{
    return m_rigidBody != nullptr;
}

IRigidBodySPtr BaseRigidGameObj::getRigidBody()
{
    return m_rigidBody;
}
