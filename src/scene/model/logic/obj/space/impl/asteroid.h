#ifndef ASTEROID_H
#define ASTEROID_H

#include <memory>

#include "../intf/asteroiddef.h"
#include "../intf/asteroidtype.h"
#include "baserigidgameobj.h"

class Asteroid : public BaseRigidGameObj
{
public:
    Asteroid(IRigidBodySPtr rigidBody, const AsteroidDef& def);
    ~Asteroid() override {}

private:
    float m_albedo;
    AsteroidType m_type;
};

using AsteroidUPtr = std::unique_ptr<Asteroid>;
using AsteroidSPtr = std::shared_ptr<Asteroid>;

#endif // ASTEROID_H
