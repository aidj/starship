#include "tinyasteroid.h"

TinyAsteroid::TinyAsteroid(IRigidBodySPtr rigidBody, AsteroidType type)
    : BaseRigidGameObj(rigidBody), m_type(type)
{
}
