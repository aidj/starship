#ifndef TINYASTEROID_H
#define TINYASTEROID_H

#include "../intf/asteroidtype.h"
#include "../../../../physics/intf/irigidbody.h"
#include "baserigidgameobj.h"

class TinyAsteroid : public BaseRigidGameObj
{
public:
    TinyAsteroid(IRigidBodySPtr rigidBody,
                 AsteroidType type);
    ~TinyAsteroid() override {}

private:
    AsteroidType m_type;
};

#endif // TINYASTEROID_H
