#include "asteroid.h"

Asteroid::Asteroid(IRigidBodySPtr rigidBody, const AsteroidDef &def)
    : BaseRigidGameObj(rigidBody),
      m_albedo(def.albedo), m_type(def.type){

}
