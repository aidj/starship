#ifndef STARSHIP_H
#define STARSHIP_H

#include <memory>
#include <map>

#include "../../space/impl/baserigidgameobj.h"
#include "../../../../physics/intf/rigidbodydef.h"
#include "thruster.h"

class Starship : public BaseRigidGameObj
{
public:
    static RigidBodyDef createRigidBodyDef(Point position, float angle);

    Starship(IRigidBodySPtr rigidBody);
    ~Starship() override {}

    const std::map<std::string, Thruster>& thrusters() const;
    void turnThruster(const std::string& thruster, bool on);
    void step();

private:
    static std::map<std::string, Thruster> createThrusters();

    std::map<std::string, Thruster> m_thrusters;
};

using StarshipSPtr = std::shared_ptr<Starship>;
using StarshipUPtr = std::unique_ptr<Starship>;

#endif // STARSHIP_H
