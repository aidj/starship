#include "starship.h"

#include <cmath>

#include "QDebug"

Starship::Starship(IRigidBodySPtr rigidBody)
    : BaseRigidGameObj(rigidBody),
      m_thrusters(std::move(createThrusters()))
{
}

const std::map<std::string, Thruster>& Starship::thrusters() const
{
    return m_thrusters;
}

void Starship::turnThruster(const std::string& thruster, bool on)
{
    m_thrusters.at(thruster).m_on = on;
}

void Starship::step()
{
    auto position = m_rigidBody->getPosition();
    auto angle = m_rigidBody->getAngle();
    Transformation2 translate(CGAL::TRANSLATION, position - CGAL::ORIGIN);
    Transformation2 rotate(CGAL::ROTATION, sin(angle), cos(angle));

    for(const auto& thruster : m_thrusters) {
        if(thruster.second.m_on) {
            auto impulse = thruster.second.getImpulse();
            auto rotatedPoint = rotate(thruster.second.m_position);
            auto rotatedVec = rotate(thruster.second.getImpulse());
            auto translatedPoint = translate(rotatedPoint);

            m_rigidBody->applyForce(
                        /*impulse,
                        thruster.second.m_position*/
                        rotate(thruster.second.getImpulse()),
                        translate(rotate(thruster.second.m_position)));
        }
    }
    qDebug() << "Velocity: (" <<
                m_rigidBody->getLinearVelocity().x() << ", " <<
                m_rigidBody->getLinearVelocity().y() << ") " <<
                m_rigidBody->getAngularVelocity();
}

RigidBodyDef Starship::createRigidBodyDef(Point position, float angle)
{
    RigidBodyDef defStarship;
    defStarship.density = 1.0f;
    defStarship.friction = 0.3f;
    Shape shape;
    shape.add(Point(-1.0f, 1.0f));
    shape.add(Point(-1.0f, -2.0f));
    shape.add(Point(1.0f, -2.0f));
    shape.add(Point(1.0f, 1.0f));
    shape.add(Point(0.0f, 2.0f));
    shape.scale(10.0f);
    defStarship.shape = std::move(shape);
    defStarship.position = position;
    defStarship.angle = angle;
    defStarship.angularVelocity = 0.0f;
    return defStarship;
}

std::map<std::string, Thruster> Starship::createThrusters()
{
    std::map<std::string, Thruster> ret;
    float scale = 10000.0f;
    ret.emplace("bottom1", Thruster(Direction2(0.0f, 1.0f), Point(-1.0f, -2.0f), 10.0f * scale));
    ret.emplace("bottom2", Thruster(Direction2(0.0f, 1.0f), Point(1.0f, -2.0f), 10.0f * scale));
    ret.emplace("right", Thruster(Direction2(-1.0f, 0.0f), Point(1.0f, 1.0f), 2.0f * scale));
    ret.emplace("left", Thruster(Direction2(1.0f, 0.0f), Point(-1.0f, 1.0f), 2.0f * scale));
    ret.emplace("top", Thruster(Direction2(0.0f, -1.0f), Point(0.0f, 2.0f), 5.0f * scale));
    return ret;
}
