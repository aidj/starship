#ifndef THRUSTER_H
#define THRUSTER_H

#include "../../../../../../common/geometry/vec2.h"

class Thruster
{
public:
    Thruster(Direction2 direction, Point position, float thrust);

    Vec2 getImpulse() const;
    Direction2 m_direction;
    Point m_position;
    float m_thrust;
    bool m_on;
};

#endif // THRUSTER_H
