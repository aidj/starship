#include "thruster.h"

Thruster::Thruster(Direction2 direction, Point position, float thrust)
    : m_direction(direction),
      m_position(position),
      m_thrust(thrust),
      m_on()
{
}

Vec2 Thruster::getImpulse() const
{
    Transformation2 scale(CGAL::SCALING, m_thrust);
    return scale(m_direction.to_vector());
}
