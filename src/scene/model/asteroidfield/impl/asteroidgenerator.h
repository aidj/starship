#ifndef ASTEROIDGENERATOR_H
#define ASTEROIDGENERATOR_H

#include "../intf/iasteroidgenerator.h"
#include "../probability_distribution/intf/igenerator.h"
#include "../probability_distribution/intf/iasteroidvelocitygenerator.h"

class AsteroidGenerator : public IAsteroidGenerator
{
public:
    AsteroidGenerator(
            IGeneratorUPtr<float, AsteroidType> asteroidAlbedoGenerator,
            IGeneratorUPtr<float> asteroidDensityGenerator,
            IGeneratorUPtr<float> asteroidFrictionGenerator,
            IGeneratorUPtr<Shape, float> asteroidShapeGenerator,
            IGeneratorUPtr<float> asteroidSizeGenerator,
            IGeneratorUPtr<AsteroidType> asteroidTypeGenerator,
            IAsteroidVelocityGeneratorUPtr asteroidVelocityGenerator);
    ~AsteroidGenerator() override {}

    // IAsteroidGenerator interface
    std::pair<AsteroidDef, RigidBodyDef> generate() override;

private:
    IGeneratorUPtr<float, AsteroidType> m_asteroidAlbedoGenerator;
    IGeneratorUPtr<float> m_asteroidDensityGenerator;
    IGeneratorUPtr<float> m_asteroidFrictionGenerator;
    IGeneratorUPtr<Shape, float> m_asteroidShapeGenerator;
    IGeneratorUPtr<float> m_asteroidSizeGenerator;
    IGeneratorUPtr<AsteroidType> m_asteroidTypeGenerator;
    IAsteroidVelocityGeneratorUPtr m_asteroidVelocityGenerator;
};

#endif // ASTEROIDGENERATOR_H
