#include "asteroidgenerator.h"

AsteroidGenerator::AsteroidGenerator(
        IGeneratorUPtr<float, AsteroidType> asteroidAlbedoGenerator,
        IGeneratorUPtr<float> asteroidDensityGenerator,
        IGeneratorUPtr<float> asteroidFrictionGenerator,
        IGeneratorUPtr<Shape, float> asteroidShapeGenerator,
        IGeneratorUPtr<float> asteroidSizeGenerator,
        IGeneratorUPtr<AsteroidType> asteroidTypeGenerator,
        IAsteroidVelocityGeneratorUPtr asteroidVelocityGenerator)
    : m_asteroidAlbedoGenerator(std::move(asteroidAlbedoGenerator)),
      m_asteroidDensityGenerator(std::move(asteroidDensityGenerator)),
      m_asteroidFrictionGenerator(std::move(asteroidFrictionGenerator)),
      m_asteroidShapeGenerator(std::move(asteroidShapeGenerator)),
      m_asteroidSizeGenerator(std::move(asteroidSizeGenerator)),
      m_asteroidTypeGenerator(std::move(asteroidTypeGenerator)),
      m_asteroidVelocityGenerator(std::move(asteroidVelocityGenerator))
{
}

std::pair<AsteroidDef, RigidBodyDef> AsteroidGenerator::generate()
{
    std::pair<AsteroidDef, RigidBodyDef> defs;
    defs.first.type = m_asteroidTypeGenerator->generate();
    defs.first.albedo = m_asteroidAlbedoGenerator->generate(defs.first.type);
    defs.second.density = m_asteroidDensityGenerator->generate();
    defs.second.friction = m_asteroidFrictionGenerator->generate();
    auto scaleFactor = m_asteroidSizeGenerator->generate();
    defs.second.shape = std::move(m_asteroidShapeGenerator->generate(scaleFactor));
    auto velocity = m_asteroidVelocityGenerator->generate(scaleFactor);
    defs.second.linearVelocity = velocity.first;
    defs.second.angularVelocity = velocity.second;
    return defs;
}
