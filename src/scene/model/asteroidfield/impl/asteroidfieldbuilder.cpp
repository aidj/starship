#include "asteroidfieldbuilder.h"

#include "../../../../common/geometry/random.h"

AsteroidFieldBuilder::AsteroidFieldBuilder(IAsteroidGeneratorSPtr generator,
        float maxWidth, float maxHeight,
        unsigned int asteroidsCount)
    : m_generator(generator),
      m_maxWidth(maxWidth),
      m_maxHeight(maxHeight),
      m_asteroidsCount(asteroidsCount)
{
}

void AsteroidFieldBuilder::build(IModelSPtr model)
{
    for(auto i = 0u; i < m_asteroidsCount; ++i) {
        auto defs = std::move(m_generator->generate());
        defs.second.position = Point(Random::random(0, m_maxWidth), Random::random(0, m_maxHeight));
        defs.second.angle = Random::random(-360.0f, 360.0f);
        // TODO: check overlap
        model->addAsteroid(
                    std::move(defs.first),
                    std::move(defs.second));
    }

    model->addStarship(std::move(
                           Starship::createRigidBodyDef(
                               Point(m_maxWidth / 2.0f, -1000.0f), 0.0f)));
}
