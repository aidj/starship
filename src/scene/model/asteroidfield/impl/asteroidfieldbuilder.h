#ifndef ASTEROIDFIELDBUILDER_H
#define ASTEROIDFIELDBUILDER_H

#include "../intf/iasteroidgenerator.h"
#include "../../intf/imodelbuilder.h"

class AsteroidFieldBuilder : public IModelBuilder
{
public:
    AsteroidFieldBuilder(
            IAsteroidGeneratorSPtr generator,
            float maxWidth,
            float maxHeight,
            unsigned int asteroidsCount);
    ~AsteroidFieldBuilder() override {}

    // IModelBuilder interface
    void build(IModelSPtr model) override;

private:
    IAsteroidGeneratorSPtr m_generator;
    float m_maxWidth;
    float m_maxHeight;
    unsigned int m_asteroidsCount;
};

#endif // ASTEROIDFIELDBUILDER_H
