#include "asteroidshapegenerator.h"

AsteroidShapeGenerator::AsteroidShapeGenerator()
{
}

Shape AsteroidShapeGenerator::generate(float scaleFactor)
{
    return Shape(s_baseShape, scaleFactor);
}

Shape AsteroidShapeGenerator::getBaseShape()
{
    Shape shape;
    shape.add(Point(1.0f, 1.0f));
    shape.add(Point(-1.0f, 1.0f));
    shape.add(Point(-1.0f, -1.0f));
    shape.add(Point(1.0f, -1.0f));
    return shape;
}

const Shape AsteroidShapeGenerator::s_baseShape =  std::move(getBaseShape());
