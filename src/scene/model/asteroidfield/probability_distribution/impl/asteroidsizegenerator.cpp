#include "asteroidsizegenerator.h"

#include "../../.././../../common/geometry/random.h"

AsteroidSizeGenerator::AsteroidSizeGenerator()
    : m_interval(Interval(0.1f, 10.0f))
{
}

float AsteroidSizeGenerator::generate()
{
    return Random::random(m_interval.first, m_interval.second);
}
