#ifndef ASTEROIDVELOCITYGENERATOR_H
#define ASTEROIDVELOCITYGENERATOR_H

#include "../../../../../common/geometry/vec3.h"
#include "../intf/commontype.h"
#include "../intf/iasteroidvelocitygenerator.h"

class AsteroidVelocityGenerator : public IAsteroidVelocityGenerator
{
public:
    AsteroidVelocityGenerator();
    ~AsteroidVelocityGenerator() override {}

    std::pair<Vec2, float> generate(float sizeFactor) override;

private:
    struct Intervals {
        Interval xVelocityInterval;
        Interval yVelocityInterval;
        Interval angularVelocityInterval;
    };

    static Intervals getIntervals();

    Intervals m_intervals;
};

#endif // ASTEROIDVELOCITYGENERATOR_H
