#ifndef ASTEROIDFRICTIONGENERATOR_H
#define ASTEROIDFRICTIONGENERATOR_H

#include "../intf/igenerator.h"

class AsteroidFrictionGenerator : public IGenerator<float>
{
public:
    AsteroidFrictionGenerator();
    ~AsteroidFrictionGenerator() override {}

    float generate() override;

private:
    using Interval = std::pair<float, float>;

    Interval m_interval;
};

#endif // ASTEROIDFRICTIONGENERATOR_H
