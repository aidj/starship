#include "asteroidtypegenerator.h"

#include "../../.././../../common/geometry/random.h"

AsteroidTypeGenerator::AsteroidTypeGenerator()
    : m_typeProbabilities(std::move(getTypeProbabilities()))
{
}

std::map<AsteroidType, float>
AsteroidTypeGenerator::getTypeProbabilities()
{
    std::map<AsteroidType, float> typeProbabilities;
    typeProbabilities[AsteroidType::CARBON] = 0.75f;
    typeProbabilities[AsteroidType::SILICATE] = 0.17f;
    typeProbabilities[AsteroidType::METAL] = 0.08f;
    return typeProbabilities;
}

AsteroidType AsteroidTypeGenerator::generate()
{
    float rand = Random::randomProbability();

    float sum = 0.0f;
    for(auto value : m_typeProbabilities) {
        sum += value.second;
        if(rand <= sum) {
            return value.first;
        }
    }
}
