#include "asteroidvelocitygenerator.h"

#include "../../../../../common/geometry/random.h"

AsteroidVelocityGenerator::AsteroidVelocityGenerator()
    : m_intervals(std::move(getIntervals()))
{
}

AsteroidVelocityGenerator::Intervals AsteroidVelocityGenerator::getIntervals()
{
    Intervals intervals;
    intervals.xVelocityInterval = Interval(-10000.0f, 10000.0f);
    intervals.yVelocityInterval = Interval(-10000.0f, 10000.0f);
    intervals.angularVelocityInterval = Interval(-10.0f, 10.0f);
    return intervals;
}

std::pair<Vec2, float> AsteroidVelocityGenerator::generate(float sizeFactor)
{
    std::pair<Vec2, float> ret(
                Vec2(
                    Random::random(m_intervals.xVelocityInterval.first, m_intervals.xVelocityInterval.second),
                    Random::random(m_intervals.yVelocityInterval.first, m_intervals.yVelocityInterval.second)
                    ),
                Random::random(m_intervals.angularVelocityInterval.first, m_intervals.angularVelocityInterval.second));
    return ret;
}
