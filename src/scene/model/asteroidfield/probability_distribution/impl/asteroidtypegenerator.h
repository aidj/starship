#ifndef ASTEROIDTYPEGENERATOR_H
#define ASTEROIDTYPEGENERATOR_H

#include <map>

#include "../intf/igenerator.h"
#include "../../../logic/obj/space/intf/asteroidtype.h"

class AsteroidTypeGenerator : public IGenerator<AsteroidType>
{
public:
    AsteroidTypeGenerator();
    ~AsteroidTypeGenerator() override {}

    AsteroidType generate() override;

private:
    static std::map<AsteroidType, float> getTypeProbabilities();

    const std::map<AsteroidType, float> m_typeProbabilities;
};


#endif // ASTEROIDTYPEGENERATOR_H
