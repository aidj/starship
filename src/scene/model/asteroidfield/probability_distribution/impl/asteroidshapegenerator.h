#ifndef ASTEROIDSHAPEGENERATOR_H
#define ASTEROIDSHAPEGENERATOR_H

#include "../intf/igenerator.h"
#include "../../../../../common/geometry/shape.h"

class AsteroidShapeGenerator : public IGenerator<Shape, float>
{
public:
    AsteroidShapeGenerator();
    ~AsteroidShapeGenerator() override {}

    Shape generate(float scaleFactor) override;

private:
    static Shape getBaseShape();

    static const Shape s_baseShape;
};

#endif // ASTEROIDSHAPEGENERATOR_H
