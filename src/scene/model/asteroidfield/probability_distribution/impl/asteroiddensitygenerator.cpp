#include "asteroiddensitygenerator.h"

#include "../../.././../../common/geometry/random.h"

AsteroidDensityGenerator::AsteroidDensityGenerator()
    : m_interval(Interval(0.0001f, 1.0f))
{
}

float AsteroidDensityGenerator::generate()
{
    return Random::random(m_interval.first, m_interval.second);
}
