#ifndef ASTEROIDSIZEGENERATOR_H
#define ASTEROIDSIZEGENERATOR_H

#include "../intf/igenerator.h"
#include "../intf/commontype.h"

class AsteroidSizeGenerator : public IGenerator<float>
{
public:
    AsteroidSizeGenerator();
    ~AsteroidSizeGenerator() override {}

    float generate() override;

private:
    Interval m_interval;
};

#endif // ASTEROIDSIZEGENERATOR_H
