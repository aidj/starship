#include "asteroidalbedogenerator.h"

#include "../../.././../../common/geometry/random.h"

AsteroidAlbedoGenerator::AsteroidAlbedoGenerator()
    : m_albedoProbabilities(std::move(getAlbedoProbabilities()))
{
}

std::map<AsteroidType, Interval>
AsteroidAlbedoGenerator::getAlbedoProbabilities()
{
    std::map<AsteroidType, Interval> albedoProbabilities;
    albedoProbabilities[AsteroidType::CARBON] = Interval(0.03f, 0.1f);
    albedoProbabilities[AsteroidType::SILICATE] = Interval(0.03f, 0.1f);
    albedoProbabilities[AsteroidType::METAL] = Interval(0.03f, 0.1f);
    return albedoProbabilities;
}

float AsteroidAlbedoGenerator::generate(AsteroidType type)
{
    auto interval = m_albedoProbabilities.at(type);
    return Random::random(interval.first, interval.second);
}
