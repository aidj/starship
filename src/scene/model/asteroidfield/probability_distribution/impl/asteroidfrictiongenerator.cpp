#include "asteroidfrictiongenerator.h"

#include "../../.././../../common/geometry/random.h"

AsteroidFrictionGenerator::AsteroidFrictionGenerator()
    : m_interval(Interval(0.0001f, 1.0f))
{
}

float AsteroidFrictionGenerator::generate()
{
    return Random::random(m_interval.first, m_interval.second);
}
