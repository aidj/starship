#ifndef ASTEROIDALBEDOGENERATOR_H
#define ASTEROIDALBEDOGENERATOR_H

#include <map>

#include "../intf/igenerator.h"
#include "../../../logic/obj/space/intf/asteroidtype.h"
#include "../intf/commontype.h"

class AsteroidAlbedoGenerator : public IGenerator<float, AsteroidType>
{
public:
    AsteroidAlbedoGenerator();
    ~AsteroidAlbedoGenerator() override {}

    float generate(AsteroidType type) override;

private:
    static std::map<AsteroidType, Interval> getAlbedoProbabilities();

    const std::map<AsteroidType, Interval> m_albedoProbabilities;
};

#endif // ASTEROIDALBEDOGENERATOR_H
