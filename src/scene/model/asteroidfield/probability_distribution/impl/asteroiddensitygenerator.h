#ifndef ASTEROIDDENSITYGENERATOR_H
#define ASTEROIDDENSITYGENERATOR_H

#include "../intf/igenerator.h"
#include "../intf/commontype.h"

class AsteroidDensityGenerator : public IGenerator<float>
{
public:
    AsteroidDensityGenerator();
    ~AsteroidDensityGenerator() override {}

    float generate() override;

private:
    Interval m_interval;
};

#endif // ASTEROIDDENSITYGENERATOR_H
