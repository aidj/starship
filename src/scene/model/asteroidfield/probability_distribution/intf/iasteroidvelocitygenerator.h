#ifndef IASTEROIDVELOCITYGENERATOR_H
#define IASTEROIDVELOCITYGENERATOR_H

#include "../intf/igenerator.h"
#include "../../../../../common/geometry/vec2.h"

using IAsteroidVelocityGenerator = IGenerator<std::pair<Vec2, float>, float>;
using IAsteroidVelocityGeneratorUPtr = std::unique_ptr<IAsteroidVelocityGenerator>;

#endif // IASTEROIDVELOCITYGENERATOR_H
