#ifndef IGENERATOR_H
#define IGENERATOR_H

#include <memory>

template<typename ReturnType, typename... Args>
class IGenerator
{
public:
    virtual ~IGenerator() {}

    virtual ReturnType generate(Args... args) = 0;
};

template<typename ReturnType, typename... Args>
using IGeneratorUPtr = std::unique_ptr<IGenerator<ReturnType, Args...>>;

#endif // IGENERATOR_H
