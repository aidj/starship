#ifndef IASTEROIDGENERATOR_H
#define IASTEROIDGENERATOR_H

#include <memory>

#include "../../logic/obj/space/intf/asteroiddef.h"
#include "../../physics/intf/rigidbodydef.h"

class IAsteroidGenerator
{
public:
    virtual ~IAsteroidGenerator() {}

    virtual std::pair<AsteroidDef, RigidBodyDef> generate() = 0;
};

using IAsteroidGeneratorSPtr = std::shared_ptr<IAsteroidGenerator>;

#endif // IASTEROIDGENERATOR_H
