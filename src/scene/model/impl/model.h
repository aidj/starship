#ifndef MODEL_H
#define MODEL_H

#include <memory>

#include "../intf/imodel.h"
#include "../physics/intf/iphysicalmodel.h"
#include "../logic/intf/ilogicalmodel.h"

class Model : public IModel
{
public:
    Model(IPhysicalModelUPtr physicalScene,
          ILogicalModelUPtr logicalScene);

    // IModel interface
    void addAsteroid(const AsteroidDef& asteroidDef, const RigidBodyDef& rigidBodyDef) override;
    void addStarship(const RigidBodyDef&) override;
    StarshipSPtr getStarship() override;

    const std::deque<IGameObjSPtr>& getGameObjs() override;

    // IEngine interface
    void step() override;

private:
    IPhysicalModelUPtr m_physicalScene;
    ILogicalModelUPtr m_logicalScene;
};

#endif // MODEL_H
