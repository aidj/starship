#include "modelcreator.h"

#include <memory>

#include "model.h"

ModelCreator::ModelCreator(
        ILogicalModelCreatorUPtr logicalModelCreator,
        IPhysicalModelCreatorUPtr physicalModelCreator,
        IModelBuilderUPtr modelBuilder)
    : m_logicalModelCreator(std::move(logicalModelCreator)),
      m_physicalModelCreator(std::move(physicalModelCreator)),
      m_modelBuilder(std::move(modelBuilder))
{
}

IModelSPtr ModelCreator::create()
{
    auto model = std::make_shared<Model>(
                 m_physicalModelCreator->create(),
                 m_logicalModelCreator->create());
    m_modelBuilder->build(model);
    return model;
}
