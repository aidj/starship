#ifndef MODELCREATOR_H
#define MODELCREATOR_H

#include "../../../common/intf/icreator.h"
#include "../intf/imodel.h"
#include "../logic/intf/ilogicalmodel.h"
#include "../physics/intf/iphysicalmodel.h"
#include "../intf/imodelbuilder.h"

using ILogicalModelCreatorUPtr = ICreatorUPtr<ILogicalModelUPtr>;
using IPhysicalModelCreatorUPtr = ICreatorUPtr<IPhysicalModelUPtr>;

class ModelCreator : public ICreator<IModelSPtr>
{
public:
    ModelCreator(ILogicalModelCreatorUPtr logicalModelCreator,
            IPhysicalModelCreatorUPtr physicalModelCreator,
            IModelBuilderUPtr modelBuilder);
    ~ModelCreator() override {}

    // ICreator interface
    IModelSPtr create() override;

private:
    ILogicalModelCreatorUPtr m_logicalModelCreator;
    IPhysicalModelCreatorUPtr m_physicalModelCreator;
    IModelBuilderUPtr m_modelBuilder;
};

#endif // MODELCREATOR_H
