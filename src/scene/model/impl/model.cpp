#include "model.h"

#include "../../../scene/model/logic/obj/intf/igameobj.h"
#include "../../../scene/model/logic/obj/space/impl/tinyasteroid.h"
#include "../../../scene/model/logic/obj/space/impl/asteroid.h"
#include "../../../scene/model/logic/obj/startship/impl/starship.h"

Model::Model(
        IPhysicalModelUPtr physicalScene,
        ILogicalModelUPtr logicalScene)
    : m_physicalScene(std::move(physicalScene)),
      m_logicalScene(std::move(logicalScene))
{
}

void Model::addAsteroid(const AsteroidDef& asteroidDef, const RigidBodyDef& rigidBodyDef)
{
    auto rigidBody = m_physicalScene->add(rigidBodyDef);
    auto asteroid = std::make_unique<Asteroid>(rigidBody, asteroidDef);
    auto gameObj = m_logicalScene->add(std::move(asteroid));
    ModelChangeEvent event(gameObj, ModelChangeEvent::CREATE);
    notify(event);
}

void Model::addStarship(const RigidBodyDef& rigidBodyDef)
{
    auto rigidBody = m_physicalScene->add(rigidBodyDef);
    auto starship = std::make_unique<Starship>(rigidBody);
    auto gameObj = m_logicalScene->setStarship(std::move(starship));
    ModelChangeEvent event(gameObj, ModelChangeEvent::CREATE);
    notify(event);
}

StarshipSPtr Model::getStarship()
{
    return m_logicalScene->getStarship();
}

const std::deque<IGameObjSPtr> &Model::getGameObjs()
{
    return m_logicalScene->getGameObjs();
}

void Model::step()
{
    m_physicalScene->step();
    m_logicalScene->step();
}
