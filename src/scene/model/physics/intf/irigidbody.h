#ifndef IRIGIDBODY_H
#define IRIGIDBODY_H

#include <memory>

#include "../../../../common/geometry/vec2.h"
#include "../../../../common/geometry/vec3.h"
#include "../../../../common/geometry/shape.h"

class IRigidBody
{
public:
    virtual ~IRigidBody() {}

    virtual Point getPosition() const = 0;
    virtual float getAngle() const = 0;
    virtual Vec2 getLinearVelocity() const = 0;
    virtual float getAngularVelocity() const = 0;
    virtual const Shape& getShape() const = 0;
    virtual float getMass() const = 0;
    virtual void applyForce(Vec2 force, Point position) = 0;
};

using IRigidBodySPtr = std::shared_ptr<IRigidBody>;

#endif // IRIGIDBODY_H
