#include "rigidbodydef.h"

RigidBodyDef::RigidBodyDef(
        const Point &position,
        float angle,
        const Vec2& linearVelocity,
        float angularVelocity,
        Shape shape,
        float density,
        float friction)
    : position(position),
      angle(angle),
      linearVelocity(linearVelocity),
      angularVelocity(angularVelocity),
      shape(std::move(shape)),
      density(density),
      friction(friction)
{
}
