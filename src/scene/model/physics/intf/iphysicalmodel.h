#ifndef IPHYSICALMODEL_H
#define IPHYSICALMODEL_H

#include <memory>

#include "rigidbodydef.h"
#include "../../../../common/intf/iengine.h"
#include "irigidbody.h"


class IPhysicalModel : public IEngine
{
public:
    IPhysicalModel() : IEngine() {}
    ~IPhysicalModel() override {}

    virtual IRigidBodySPtr add(const RigidBodyDef& rigidBodyDefy) = 0;
};

using IPhysicalModelUPtr = std::unique_ptr<IPhysicalModel>;

#endif // IPHYSICALMODEL_H
