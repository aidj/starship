#ifndef RIGIDBODYDEF_H
#define RIGIDBODYDEF_H

#include <memory>

#include "../../../../common/geometry/vec2.h"
#include "../../../../common/geometry/vec3.h"
#include "../../../../common/geometry/shape.h"

struct RigidBodyDef
{
    RigidBodyDef() = default;
    RigidBodyDef ( RigidBodyDef && rigidBodyDef) = default;

    RigidBodyDef(
            const Point& position,
            float angle,
            const Vec2& linearVelocity,
            float angularVelocity,
            Shape shape,
            float density,
            float friction);

    Point position;
    float angle;
    Vec2 linearVelocity;
    float angularVelocity;
    Shape shape;
    float density;
    float friction;
};

using RigidBodyDefStr = std::shared_ptr<RigidBodyDef>;

#endif // RIGIDBODYDEF_H
