#ifndef BOX2DRIGIDBODY_H
#define BOX2DRIGIDBODY_H

#include "Box2D/Box2D.h"

#include "../intf/irigidbody.h"

class Box2DRigidBody : public IRigidBody
{
public:
    Box2DRigidBody(Shape shape, b2Body* body)
        : m_shape(std::move(shape)),
          m_body(body) {}
    ~Box2DRigidBody() override {
        // TODO: check how to manage m_body
    }

    // IRigidBody interface
    Point getPosition() const override;
    float getAngle() const override;
    Vec2 getLinearVelocity() const override;
    float getAngularVelocity() const override;
    const Shape &getShape() const override;
    float getMass() const override;
    void applyForce(Vec2 force, Point applyPoint) override;

private:
    Shape m_shape;
    b2Body* m_body;
};

#endif // BOX2DRIGIDBODY_H
