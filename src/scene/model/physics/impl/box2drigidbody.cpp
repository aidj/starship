#include "box2drigidbody.h"

Point Box2DRigidBody::getPosition() const
{
    return Point(m_body->GetPosition().x, m_body->GetPosition().y);
}

float Box2DRigidBody::getAngle() const
{
    return m_body->GetAngle();
}

Vec2 Box2DRigidBody::getLinearVelocity() const
{
    return Vec2(m_body->GetLinearVelocity().x, m_body->GetLinearVelocity().y);
}

float Box2DRigidBody::getAngularVelocity() const
{
    return m_body->GetAngularVelocity();
}

const Shape &Box2DRigidBody::getShape() const
{
    return m_shape;
}

float Box2DRigidBody::getMass() const
{
    return m_body->GetMass();
}

void Box2DRigidBody::applyForce(Vec2 force, Point position)
{
    // Check is it really needed to wake up a body.
    m_body->ApplyForce(b2Vec2(force.x(), force.y()),
                       b2Vec2(position.x(), position.y()), true);
}
