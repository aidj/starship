#include "box2dphysicalmodelcreator.h"

#include "box2dphysicalmodel.h"

IPhysicalModelUPtr Box2DPhysicalModelCreator::create()
{
    return std::make_unique<Box2DPhysicalModel>();
}
