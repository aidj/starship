#ifndef BOX2DPHYSICALMODELCREATOR_H
#define BOX2DPHYSICALMODELCREATOR_H

#include "../../../../common/intf/icreator.h"
#include "../intf/iphysicalmodel.h"

class Box2DPhysicalModelCreator : public ICreator<IPhysicalModelUPtr>
{
public:
    Box2DPhysicalModelCreator() {}
    ~Box2DPhysicalModelCreator() override {}

    // ICreator interface
    IPhysicalModelUPtr create() override;
};

#endif // BOX2DPHYSICALMODELCREATOR_H
