#ifndef BOX2DPHYSICALMODEL_H
#define BOX2DPHYSICALMODEL_H

#include <memory>
#include <list>

#include "Box2D/Box2D.h"

#include "../intf/iphysicalmodel.h"
#include "../intf/rigidbodydef.h"

class Box2DPhysicalModel : public IPhysicalModel
{
public:
    Box2DPhysicalModel();
    ~Box2DPhysicalModel() override;

    // IEngine interface
    void step() override;

    // IPhysicalModel interface
    IRigidBodySPtr add(const RigidBodyDef &rigidBodyDefy) override;

private:
    b2BodyDef getBodyDef(const RigidBodyDef& rigidBody);
    std::list<b2PolygonShape> shapeToPolygons(const Shape& shape);
    b2FixtureDef getFixtureDef(const RigidBodyDef& rigidBody, const b2PolygonShape& polygon);

    std::unique_ptr<b2World> m_world;
    std::list<b2Body*> m_bodies;
};

#endif // BOX2DPHYSICALMODEL_H
