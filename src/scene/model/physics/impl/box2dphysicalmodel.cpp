#include "box2dphysicalmodel.h"

#include <memory>

#include "box2drigidbody.h"

Box2DPhysicalModel::Box2DPhysicalModel()
    : IPhysicalModel(),
      m_world(nullptr), m_bodies()
{
    b2Vec2 gravity(0.0f, 0.0f);
    m_world = std::make_unique<b2World>(gravity);
}

Box2DPhysicalModel::~Box2DPhysicalModel()
{
    for(b2Body* body : m_bodies) {
        m_world->DestroyBody(body);
    }
    m_world = nullptr;
}

void Box2DPhysicalModel::step()
{
    const float32 timeStep = 1.0f / 240.0f;
    const int velocityIterations = 8;
    const int positionIterations = 3;
    m_world->Step(timeStep, velocityIterations, positionIterations);
}

IRigidBodySPtr Box2DPhysicalModel::add(const RigidBodyDef &rigidBodyDef)
{
    b2BodyDef bodyDef = getBodyDef(rigidBodyDef);
    b2Body* body = m_world->CreateBody(&bodyDef);

    std::list<b2PolygonShape> polygons = shapeToPolygons(rigidBodyDef.shape);
    for(const b2PolygonShape& polygon : polygons) {
        b2FixtureDef fixtureDef = getFixtureDef(rigidBodyDef, polygon);
        body->CreateFixture(&fixtureDef);
    }

    m_bodies.push_back(body);
    return std::make_shared<Box2DRigidBody>(rigidBodyDef.shape, body);
}

b2BodyDef Box2DPhysicalModel::getBodyDef(const RigidBodyDef& rigidBodyDef)
{
    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    bodyDef.position.Set(
                rigidBodyDef.position.x(),
                rigidBodyDef.position.y());
    bodyDef.angle = rigidBodyDef.angle;
    bodyDef.angularVelocity = rigidBodyDef.angularVelocity;
    bodyDef.linearVelocity.Set(
                rigidBodyDef.linearVelocity.x(),
                rigidBodyDef.linearVelocity.y());
    // TODO: do not forget to remove pointer safety
    //bodyDef.userData = rigidBody.get();

    return bodyDef;
}

std::list<b2PolygonShape> Box2DPhysicalModel::shapeToPolygons(const Shape& shape)
{
    std::vector<b2Vec2> vertices;
    vertices.resize(shape.points.size());

    for(size_t i = 0; i < shape.points.size(); ++i) {
        vertices[i].Set(shape.points[i].x(), shape.points[i].y());
    }

    b2PolygonShape polygon;
    polygon.Set(vertices.data(), shape.points.size());

    std::list<b2PolygonShape> polygons;
    polygons.push_back(std::move(polygon));
    return polygons;
}

b2FixtureDef Box2DPhysicalModel::getFixtureDef(
        const RigidBodyDef& rigidBodyDef,
        const b2PolygonShape& polygon)
{
    b2FixtureDef fixtureDef;
    fixtureDef.shape = &polygon;
    fixtureDef.density = rigidBodyDef.density;
    fixtureDef.friction = rigidBodyDef.friction;

    return fixtureDef;
}
