#ifndef ISCENE_H
#define ISCENE_H

#include <memory>

class IScene
{
public:
    virtual ~IScene() {}

    virtual void run() = 0;
};

using ISceneUPtr = std::unique_ptr<IScene>;

#endif // ISCENE_H
