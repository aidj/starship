#ifndef SCENEOGLDEMO_H
#define SCENEOGLDEMO_H

#include "../../common/intf/icreator.h"
#include "../../scene/intf/iscene.h"
#include "../../scene/model/intf/imodelbuilder.h"

class SceneOGLDemo : public ICreator<ICreatorUPtr<ISceneUPtr>>
{
public:
    SceneOGLDemo() {}
    ~SceneOGLDemo() override {}

    // ICreator interface
    ICreatorUPtr<ISceneUPtr> create() override;

private:
    IModelBuilderUPtr createModelBuilder();
};

#endif // SCENEOGLDEMO_H
