#include "sceneogldemo.h"

#include <memory>

#include "../../scene/impl/scenecreator.h"
#include "../../scene/model/impl/modelcreator.h"
#include "../../scene/model/logic/impl/logicalmodelcreator.h"
#include "../../scene/model/physics/impl/box2dphysicalmodelcreator.h"
#include "../../scene/view/opengl/impl/oglviewcreator.h"
#include "../../scene/controller/impl/controllercreator.h"
#include "../../scene/model/asteroidfield/impl/asteroidfieldbuilder.h"
#include "../../scene/model/asteroidfield/impl/asteroidgenerator.h"
#include "../../scene/model/asteroidfield/probability_distribution/impl/asteroidalbedogenerator.h"
#include "../../scene/model/asteroidfield/probability_distribution/impl/asteroiddensitygenerator.h"
#include "../../scene/model/asteroidfield/probability_distribution/impl/asteroidfrictiongenerator.h"
#include "../../scene/model/asteroidfield/probability_distribution/impl/asteroidshapegenerator.h"
#include "../../scene/model/asteroidfield/probability_distribution/impl/asteroidsizegenerator.h"
#include "../../scene/model/asteroidfield/probability_distribution/impl/asteroidtypegenerator.h"
#include "../../scene/model/asteroidfield/probability_distribution/impl/asteroidvelocitygenerator.h"

ICreatorUPtr<ISceneUPtr> SceneOGLDemo::create()
{
    auto logicalModelCreator = std::make_unique<LogicalModelCreator>();
    auto physicalModelCreator = std::make_unique<Box2DPhysicalModelCreator>();
    auto modelCreator = std::make_unique<ModelCreator>(
                std::move(logicalModelCreator),
                std::move(physicalModelCreator),
                std::move(createModelBuilder()));
    auto viewCreator = std::make_unique<OGLViewCreator>();
    auto controllerCreator = std::make_unique<ControllerCreator>();
    auto sceneCreator = std::make_unique<SceneCreator>(
                std::move(modelCreator),
                std::move(viewCreator),
                std::move(controllerCreator));
    return sceneCreator;
}

IModelBuilderUPtr SceneOGLDemo::createModelBuilder()
{
    auto albedoGenerator = std::make_unique<AsteroidAlbedoGenerator>();
    auto densityGenerator = std::make_unique<AsteroidDensityGenerator>();
    auto frictionGenerator = std::make_unique<AsteroidFrictionGenerator>();
    auto shapeGenerator = std::make_unique<AsteroidShapeGenerator>();
    auto sizeGenerator = std::make_unique<AsteroidSizeGenerator>();
    auto typeGenerator = std::make_unique<AsteroidTypeGenerator>();
    auto velocityGenerator = std::make_unique<AsteroidVelocityGenerator>();

    auto generator = std::make_shared<AsteroidGenerator>(
                std::move(albedoGenerator),
                std::move(densityGenerator),
                std::move(frictionGenerator),
                std::move(shapeGenerator),
                std::move(sizeGenerator),
                std::move(typeGenerator),
                std::move(velocityGenerator)
                );

    auto modelBuilder = std::make_unique<AsteroidFieldBuilder>(
                generator,
                10000.0f,
                10000.0f,
                1000u);

    return modelBuilder;
}
