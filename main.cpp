#include <QApplication>

#include "src/demo/impl/sceneogldemo.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    SceneOGLDemo demo;
    auto sceneCreator = demo.create();
    auto scene = sceneCreator->create();
    scene->run();

    return a.exec();
}
