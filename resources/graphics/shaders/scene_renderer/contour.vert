#version 450 core

in vec2 position;

out VS_OUT
{
    vec4 colour;
} vs_out;

layout (location = 0) uniform mat4 tr_matrix;

void main(void)
{
    gl_Position = tr_matrix * vec4 (position, 0.0, 1.0);
    vs_out.colour = vec4(1.0, 1.0, 1.0, 0.0);
    //gl_Position * 2.0 + vec4(0.5, 0.5, 0.5, 0.0);
}
