#version 450 core

out vec4 colour;

in VS_OUT
{
    vec4 colour;
} fs_in;

void main(void)
{
    colour = fs_in.colour;
}
