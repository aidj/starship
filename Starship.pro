#-------------------------------------------------
#
# Project created by QtCreator 2015-05-23T20:57:17
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG   += c++14
#QMAKE_CXXFLAGS += -std=c++14

TARGET = Starship
TEMPLATE = app

SOURCES += main.cpp \
    src/demo/impl/sceneogldemo.cpp \
    src/scene/model/logic/obj/space/impl/baserigidgameobj.cpp \
    src/scene/model/logic/obj/space/impl/tinyasteroid.cpp \
    src/scene/model/physics/intf/rigidbodydef.cpp \
    src/scene/view/opengl/renderer/impl/camera.cpp \
    src/scene/view/opengl/renderer/impl/renderdata.cpp \
    src/scene/view/opengl/renderer/impl/renderdataprovider.cpp \
    src/scene/view/opengl/renderer/impl/sceneoglrenderer.cpp \
    src/scene/view/opengl/renderer/impl/sceneoglrendererinputobserver.cpp \
    src/scene/view/opengl/widget/impl/ogllogger.cpp \
    src/scene/impl/scene.cpp \
    src/scene/impl/scenecreator.cpp \
    src/scene/model/impl/modelcreator.cpp \
    src/scene/controller/impl/controllercreator.cpp \
    src/scene/model/impl/model.cpp \
    src/scene/model/logic/impl/logicalmodelcreator.cpp \
    src/scene/model/logic/impl/logicalmodel.cpp \
    src/scene/model/physics/impl/box2dphysicalmodelcreator.cpp \
    src/scene/model/physics/impl/box2dphysicalmodel.cpp \
    src/scene/view/opengl/impl/oglviewcreator.cpp \
    src/scene/view/opengl/impl/oglview.cpp \
    src/scene/view/opengl/renderer/impl/baseoglrenderer.cpp \
    src/scene/impl/scenerunner.cpp \
    src/scene/view/opengl/widget/impl/inputproviderwidget.cpp \
    src/scene/view/opengl/widget/impl/oglwidget.cpp \
    src/scene/controller/impl/controller.cpp \
    src/scene/model/logic/obj/space/impl/asteroid.cpp \
    src/scene/model/asteroidfield/impl/asteroidfieldbuilder.cpp \
    src/common/geometry/random.cpp \
    src/scene/model/asteroidfield/probability_distribution/impl/asteroidtypegenerator.cpp \
    src/scene/model/asteroidfield/probability_distribution/impl/asteroidalbedogenerator.cpp \
    src/scene/model/asteroidfield/probability_distribution/impl/asteroiddensitygenerator.cpp \
    src/scene/model/asteroidfield/probability_distribution/impl/asteroidfrictiongenerator.cpp \
    src/scene/model/asteroidfield/probability_distribution/impl/asteroidsizegenerator.cpp \
    src/scene/model/asteroidfield/probability_distribution/impl/asteroidshapegenerator.cpp \
    src/scene/model/asteroidfield/probability_distribution/impl/asteroidvelocitygenerator.cpp \
    src/scene/model/asteroidfield/impl/asteroidgenerator.cpp \
    src/scene/model/logic/obj/startship/impl/starship.cpp \
    src/scene/model/physics/impl/box2drigidbody.cpp \
    src/scene/model/logic/obj/startship/impl/thruster.cpp

HEADERS  += \
    src/common/intf/iengine.h \
    src/demo/impl/sceneogldemo.h \
    src/exceptions/starshipexception.h \
    src/exceptions/oglexception.h \
    src/exceptions/invalidargument.h \
    src/common/intf/observable.h \
    src/common/intf/observer.h \
    src/exceptions/initializationerror.h \
    src/common/input/inputevent.h \
    src/common/geometry/shape.h \
    src/common/geometry/vec2.h \
    src/common/geometry/vec3.h \
    src/scene/model/logic/obj/intf/igameobj.h \
    src/scene/model/logic/obj/space/intf/asteroidtype.h \
    src/scene/model/logic/obj/space/impl/baserigidgameobj.h \
    src/scene/model/logic/obj/space/impl/tinyasteroid.h \
    src/scene/model/physics/intf/irigidbody.h \
    src/scene/model/physics/intf/rigidbodydef.h \
    src/scene/view/intf/iview.h \
    src/scene/view/opengl/intf/commonogltypes.h \
    src/scene/view/opengl/renderer/impl/camera.h \
    src/scene/view/opengl/renderer/impl/graphicalobj.h \
    src/scene/view/opengl/renderer/impl/renderdata.h \
    src/scene/view/opengl/renderer/impl/renderdataprovider.h \
    src/scene/view/opengl/renderer/impl/sceneoglrenderer.h \
    src/scene/view/opengl/renderer/impl/sceneoglrendererinputobserver.h \
    src/scene/view/opengl/widget/impl/ogllogger.h \
    src/scene/view/opengl/widget/intf/iwidget.h \
    src/scene/model/intf/imodel.h \
    src/common/intf/icreator.h \
    src/scene/impl/scene.h \
    src/scene/intf/iscene.h \
    src/scene/controller/intf/icontroller.h \
    src/scene/impl/scenecreator.h \
    src/scene/model/impl/modelcreator.h \
    src/scene/controller/impl/controllercreator.h \
    src/scene/model/impl/model.h \
    src/scene/model/logic/impl/logicalmodelcreator.h \
    src/scene/model/logic/impl/logicalmodel.h \
    src/scene/model/logic/intf/ilogicalmodel.h \
    src/scene/model/physics/impl/box2dphysicalmodelcreator.h \
    src/scene/model/physics/intf/iphysicalmodel.h \
    src/scene/model/intf/modelchangeevent.h \
    src/scene/model/physics/impl/box2dphysicalmodel.h \
    src/scene/view/opengl/impl/oglviewcreator.h \
    src/scene/view/opengl/intf/ioglcontextprovider.h \
    src/scene/view/opengl/impl/oglview.h \
    src/scene/view/opengl/renderer/impl/baseoglrenderer.h \
    src/scene/view/opengl/renderer/intf/ioglrenderer.h \
    src/scene/impl/scenerunner.h \
    src/scene/view/opengl/widget/impl/inputproviderwidget.h \
    src/scene/view/opengl/widget/impl/oglwidget.h \
    src/scene/controller/impl/controller.h \
    src/scene/model/logic/obj/space/intf/asteroiddef.h \
    src/scene/model/logic/obj/space/impl/asteroid.h \
    src/scene/model/asteroidfield/intf/asteroidfielddef.h \
    src/scene/model/asteroidfield/impl/asteroidfieldbuilder.h \
    src/common/geometry/random.h \
    src/scene/model/asteroidfield/probability_distribution/intf/igenerator.h \
    src/scene/model/asteroidfield/probability_distribution/impl/asteroidtypegenerator.h \
    src/scene/model/asteroidfield/probability_distribution/impl/asteroidalbedogenerator.h \
    src/scene/model/asteroidfield/probability_distribution/impl/asteroiddensitygenerator.h \
    src/scene/model/asteroidfield/probability_distribution/impl/asteroidfrictiongenerator.h \
    src/scene/model/asteroidfield/probability_distribution/impl/asteroidsizegenerator.h \
    src/scene/model/asteroidfield/probability_distribution/impl/asteroidshapegenerator.h \
    src/scene/model/asteroidfield/probability_distribution/impl/asteroidvelocitygenerator.h \
    src/scene/model/asteroidfield/probability_distribution/intf/commontype.h \
    src/scene/model/asteroidfield/intf/iasteroidgenerator.h \
    src/scene/model/asteroidfield/impl/asteroidgenerator.h \
    src/scene/model/intf/imodelbuilder.h \
    src/scene/model/logic/obj/startship/impl/starship.h \
    src/scene/model/physics/impl/box2drigidbody.h \
    src/scene/model/logic/obj/startship/impl/thruster.h \
    src/common/geometry/interval.h \
    src/scene/model/asteroidfield/probability_distribution/intf/iasteroidvelocitygenerator.h

FORMS    +=

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/lib/Box2D/x64/release/ -lBox2D
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/lib/Box2D/x64/debug/ -lBox2D

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/lib/Box2D/x64/release/libBox2D.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/lib/Box2D/x64/debug/libBox2D.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/lib/Box2D/x64/release/Box2D.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/lib/Box2D/x64/debug/Box2D.lib

DISTFILES += \
    resources/graphics/shaders/scene_renderer/contour.frag \
    resources/graphics/shaders/scene_renderer/contour.vert

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/lib/CGAL-4.9/x64/release/ -llibCGAL_Core-vc140-mt-4.9
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/lib/CGAL-4.9/x64/debug/ -llibCGAL_Core-vc140-mt-gd-4.9

INCLUDEPATH += $$PWD/include/CGAL-4.9
DEPENDPATH += $$PWD/include/CGAL-4.9

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/lib/CGAL-4.9/x64/release/liblibCGAL_Core-vc140-mt-4.9.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/lib/CGAL-4.9/x64/debug/liblibCGAL_Core-vc140-mt-gd-4.9.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/lib/CGAL-4.9/x64/release/libCGAL_Core-vc140-mt-4.9.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/lib/CGAL-4.9/x64/debug/libCGAL_Core-vc140-mt-gd-4.9.lib

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/lib/CGAL-4.9/x64/release/ -llibCGAL-vc140-mt-4.9
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/lib/CGAL-4.9/x64/debug/ -llibCGAL-vc140-mt-gd-4.9

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/lib/CGAL-4.9/x64/release/liblibCGAL-vc140-mt-4.9.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/lib/CGAL-4.9/x64/debug/liblibCGAL-vc140-mt-gd-4.9.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/lib/CGAL-4.9/x64/release/libCGAL-vc140-mt-4.9.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/lib/CGAL-4.9/x64/debug/libCGAL-vc140-mt-gd-4.9.lib

INCLUDEPATH += $$PWD/include/boost_1_63_0
DEPENDPATH += $$PWD/include/boost_1_63_0

Release:INCLUDEPATH += $$PWD/include/CGAL-4.9_release
Debug:INCLUDEPATH += $$PWD/include/CGAL-4.9_debug
